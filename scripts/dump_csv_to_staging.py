#!/usr/bin/python
import pandas as pd
import os 
from sqlalchemy import create_engine

def dump_csv_file_to_staging_schema(sqlalchemy_engine, csvfile: str, stagingschema: str = None) -> None:
    raw_csv_file = pd.read_csv(csvfile)  
    raw_csv_file.to_sql("csv_" + os.path.splitext(csvfile)[0],sqlalchemy_engine,schema=stagingschema,index=False)


if __name__ == "__main__":
  engine = create_engine('postgresql://postgres:postgres@test_postgres:5432/test_crdw')  
  dump_csv_file_to_staging_schema(engine, "patients.csv", "dump")