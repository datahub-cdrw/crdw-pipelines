"""Setup.py file."""

import setuptools

setuptools.setup(
    name="crdw_modules",
    version="0.1",
    description="packaged containing the crdw src logic",
    packages=setuptools.find_packages(where="src"),
    package_dir={"": "src"},
    install_requires=["apache-airflow~=1.10.2", "psycopg2-binary"],
    python_requires="==3.7.*",
)