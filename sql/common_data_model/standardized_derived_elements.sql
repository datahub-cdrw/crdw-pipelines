
--HINT DISTRIBUTE_ON_KEY(subject_id)
CREATE TABLE SCHEMA_PLACEHOLDER.cohort
(
  cohort_definition_id	INTEGER		NOT NULL ,
  subject_id						INTEGER		NOT NULL ,
  cohort_start_date			DATE			NOT NULL ,
  cohort_end_date				DATE			NOT NULL
)
;


--HINT DISTRIBUTE_ON_KEY(subject_id)
CREATE TABLE SCHEMA_PLACEHOLDER.cohort_attribute
(
  cohort_definition_id		INTEGER		NOT NULL ,
  subject_id						  INTEGER		NOT NULL ,
  cohort_start_date				DATE			NOT NULL ,
  cohort_end_date				  DATE			NOT NULL ,
  attribute_definition_id INTEGER		NOT NULL ,
  value_as_number				  NUMERIC			NULL ,
  value_as_concept_id			INTEGER		NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE SCHEMA_PLACEHOLDER.drug_era
(
  drug_era_id					INTEGER			NOT NULL ,
  person_id						INTEGER			NOT NULL ,
  drug_concept_id			INTEGER			NOT NULL ,
  drug_era_start_date	DATE			  NOT NULL ,
  drug_era_end_date		DATE			  NOT NULL ,
  drug_exposure_count	INTEGER			NULL ,
  gap_days						INTEGER			NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE SCHEMA_PLACEHOLDER.dose_era
(
  dose_era_id					  INTEGER			NOT NULL ,
  person_id						  INTEGER			NOT NULL ,
  drug_concept_id				INTEGER			NOT NULL ,
  unit_concept_id				INTEGER			NOT NULL ,
  dose_value						NUMERIC			  NOT NULL ,
  dose_era_start_date		DATE			  NOT NULL ,
  dose_era_end_date	    DATE			  NOT NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE SCHEMA_PLACEHOLDER.condition_era
(
  condition_era_id				    INTEGER			NOT NULL ,
  person_id						        INTEGER			NOT NULL ,
  condition_concept_id			  INTEGER			NOT NULL ,
  condition_era_start_date		DATE			  NOT NULL ,
  condition_era_end_date			DATE			  NOT NULL ,
  condition_occurrence_count	INTEGER			NULL
)
;

-- ALTER TABLE SCHEMA_PLACEHOLDER.cohort ADD CONSTRAINT xpk_cohort PRIMARY KEY ( cohort_definition_id, subject_id, cohort_start_date, cohort_end_date  ) ;

-- ALTER TABLE SCHEMA_PLACEHOLDER.cohort_attribute ADD CONSTRAINT xpk_cohort_attribute PRIMARY KEY ( cohort_definition_id, subject_id, cohort_start_date, cohort_end_date, attribute_definition_id ) ;

-- ALTER TABLE SCHEMA_PLACEHOLDER.drug_era ADD CONSTRAINT xpk_drug_era PRIMARY KEY ( drug_era_id ) ;

-- ALTER TABLE SCHEMA_PLACEHOLDER.dose_era  ADD CONSTRAINT xpk_dose_era PRIMARY KEY ( dose_era_id ) ;

-- ALTER TABLE SCHEMA_PLACEHOLDER.condition_era ADD CONSTRAINT xpk_condition_era PRIMARY KEY ( condition_era_id ) ;

CREATE INDEX idx_cohort_subject_id ON SCHEMA_PLACEHOLDER.cohort (subject_id ASC);
CREATE INDEX idx_cohort_c_definition_id ON SCHEMA_PLACEHOLDER.cohort (cohort_definition_id ASC);

CREATE INDEX idx_ca_subject_id ON SCHEMA_PLACEHOLDER.cohort_attribute (subject_id ASC);
CREATE INDEX idx_ca_definition_id ON SCHEMA_PLACEHOLDER.cohort_attribute (cohort_definition_id ASC);

CREATE INDEX idx_drug_era_person_id  ON SCHEMA_PLACEHOLDER.drug_era  (person_id ASC);
CLUSTER SCHEMA_PLACEHOLDER.drug_era  USING idx_drug_era_person_id ;
CREATE INDEX idx_drug_era_concept_id ON SCHEMA_PLACEHOLDER.drug_era (drug_concept_id ASC);

CREATE INDEX idx_dose_era_person_id  ON SCHEMA_PLACEHOLDER.dose_era  (person_id ASC);
CLUSTER SCHEMA_PLACEHOLDER.dose_era  USING idx_dose_era_person_id ;
CREATE INDEX idx_dose_era_concept_id ON SCHEMA_PLACEHOLDER.dose_era (drug_concept_id ASC);

CREATE INDEX idx_condition_era_person_id  ON SCHEMA_PLACEHOLDER.condition_era  (person_id ASC);
CLUSTER SCHEMA_PLACEHOLDER.condition_era  USING idx_condition_era_person_id ;
CREATE INDEX idx_condition_era_concept_id ON SCHEMA_PLACEHOLDER.condition_era (condition_concept_id ASC);