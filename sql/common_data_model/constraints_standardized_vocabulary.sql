ALTER TABLE SCHEMA_PLACEHOLDER.concept ADD CONSTRAINT fpk_concept_domain FOREIGN KEY (domain_id)  REFERENCES SCHEMA_PLACEHOLDER.domain (domain_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept ADD CONSTRAINT fpk_concept_class FOREIGN KEY (concept_class_id)  REFERENCES SCHEMA_PLACEHOLDER.concept_class (concept_class_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept ADD CONSTRAINT fpk_concept_vocabulary FOREIGN KEY (vocabulary_id)  REFERENCES SCHEMA_PLACEHOLDER.vocabulary (vocabulary_id);

ALTER TABLE SCHEMA_PLACEHOLDER.vocabulary ADD CONSTRAINT fpk_vocabulary_concept FOREIGN KEY (vocabulary_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.domain ADD CONSTRAINT fpk_domain_concept FOREIGN KEY (domain_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept_class ADD CONSTRAINT fpk_concept_class_concept FOREIGN KEY (concept_class_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept_relationship ADD CONSTRAINT fpk_concept_relationship_c_1 FOREIGN KEY (concept_id_1)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept_relationship ADD CONSTRAINT fpk_concept_relationship_c_2 FOREIGN KEY (concept_id_2)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept_relationship ADD CONSTRAINT fpk_concept_relationship_id FOREIGN KEY (relationship_id)  REFERENCES SCHEMA_PLACEHOLDER.relationship (relationship_id);

ALTER TABLE SCHEMA_PLACEHOLDER.relationship ADD CONSTRAINT fpk_relationship_concept FOREIGN KEY (relationship_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.relationship ADD CONSTRAINT fpk_relationship_reverse FOREIGN KEY (reverse_relationship_id)  REFERENCES SCHEMA_PLACEHOLDER.relationship (relationship_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept_synonym ADD CONSTRAINT fpk_concept_synonym_concept FOREIGN KEY (concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept_ancestor ADD CONSTRAINT fpk_concept_ancestor_concept_1 FOREIGN KEY (ancestor_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept_ancestor ADD CONSTRAINT fpk_concept_ancestor_concept_2 FOREIGN KEY (descendant_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.source_to_concept_map ADD CONSTRAINT fpk_source_to_concept_map_v_1 FOREIGN KEY (source_vocabulary_id)  REFERENCES SCHEMA_PLACEHOLDER.vocabulary (vocabulary_id);

ALTER TABLE SCHEMA_PLACEHOLDER.source_to_concept_map ADD CONSTRAINT fpk_source_to_concept_map_v_2 FOREIGN KEY (target_vocabulary_id)  REFERENCES SCHEMA_PLACEHOLDER.vocabulary (vocabulary_id);

ALTER TABLE SCHEMA_PLACEHOLDER.source_to_concept_map ADD CONSTRAINT fpk_source_to_concept_map_c_1 FOREIGN KEY (target_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.drug_strength ADD CONSTRAINT fpk_drug_strength_concept_1 FOREIGN KEY (drug_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.drug_strength ADD CONSTRAINT fpk_drug_strength_concept_2 FOREIGN KEY (ingredient_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.drug_strength ADD CONSTRAINT fpk_drug_strength_unit_1 FOREIGN KEY (amount_unit_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.drug_strength ADD CONSTRAINT fpk_drug_strength_unit_2 FOREIGN KEY (numerator_unit_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.drug_strength ADD CONSTRAINT fpk_drug_strength_unit_3 FOREIGN KEY (denominator_unit_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.cohort_definition ADD CONSTRAINT fpk_cohort_definition_concept FOREIGN KEY (definition_type_concept_id)  REFERENCES SCHEMA_PLACEHOLDER.concept (concept_id);

ALTER TABLE SCHEMA_PLACEHOLDER.concept_synonym ADD CONSTRAINT uq_concept_synonym UNIQUE (concept_id, concept_synonym_name, language_concept_id);
