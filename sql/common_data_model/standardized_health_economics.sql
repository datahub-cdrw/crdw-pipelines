
--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE SCHEMA_PLACEHOLDER.payer_plan_period
(
  payer_plan_period_id			    INTEGER			  NOT NULL ,
  person_id						          INTEGER			  NOT NULL ,
  payer_plan_period_start_date  DATE			    NOT NULL ,
  payer_plan_period_end_date		DATE			    NOT NULL ,
  payer_concept_id              INTEGER       NULL ,
  payer_source_value				    VARCHAR(50)	  NULL ,
  payer_source_concept_id       INTEGER       NULL ,
  plan_concept_id               INTEGER       NULL ,
  plan_source_value				      VARCHAR(50)	  NULL ,
  plan_source_concept_id        INTEGER       NULL ,
  sponsor_concept_id            INTEGER       NULL ,
  sponsor_source_value          VARCHAR(50)   NULL ,
  sponsor_source_concept_id     INTEGER       NULL ,
  family_source_value			      VARCHAR(50)	  NULL ,
  stop_reason_concept_id        INTEGER       NULL ,
  stop_reason_source_value      INTEGER       NULL ,
  stop_reason_source_concept_id INTEGER       NULL
)
;


CREATE TABLE SCHEMA_PLACEHOLDER.cost
(
  cost_id					          INTEGER	    NOT NULL ,
  cost_event_id             INTEGER     NOT NULL ,
  cost_domain_id            VARCHAR(20) NOT NULL ,
  cost_type_concept_id      INTEGER     NOT NULL ,
  currency_concept_id			  INTEGER			NULL ,
  total_charge						  NUMERIC			  NULL ,
  total_cost						    NUMERIC			  NULL ,
  total_paid						    NUMERIC			  NULL ,
  paid_by_payer					    NUMERIC			  NULL ,
  paid_by_patient						NUMERIC			  NULL ,
  paid_patient_copay				NUMERIC			  NULL ,
  paid_patient_coinsurance  NUMERIC			  NULL ,
  paid_patient_deductible		NUMERIC			  NULL ,
  paid_by_primary						NUMERIC			  NULL ,
  paid_ingredient_cost			NUMERIC			  NULL ,
  paid_dispensing_fee				NUMERIC			  NULL ,
  payer_plan_period_id			INTEGER			NULL ,
  amount_allowed		        NUMERIC			  NULL ,
  revenue_code_concept_id		INTEGER			NULL ,
  reveue_code_source_value  VARCHAR(50) NULL,
  drg_concept_id			      INTEGER		  NULL,
  drg_source_value			    VARCHAR(3)	NULL
)
;

-- ALTER TABLE SCHEMA_PLACEHOLDER.payer_plan_period ADD CONSTRAINT xpk_payer_plan_period PRIMARY KEY ( payer_plan_period_id ) ;

-- ALTER TABLE SCHEMA_PLACEHOLDER.cost ADD CONSTRAINT xpk_visit_cost PRIMARY KEY ( cost_id ) ;

CREATE INDEX idx_period_person_id  ON SCHEMA_PLACEHOLDER.payer_plan_period  (person_id ASC);
CLUSTER SCHEMA_PLACEHOLDER.payer_plan_period  USING idx_period_person_id ;