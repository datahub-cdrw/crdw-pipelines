CREATE DATABASE airflow;
CREATE DATABASE test_crdw; 
\connect test_crdw
CREATE SCHEMA vocab;
CREATE SCHEMA cdm;
CREATE SCHEMA dm_synthea10;
CREATE SCHEMA staging_dm_synthea10;
CREATE SCHEMA staging;
CREATE SCHEMA staging_cdm_syntax;
CREATE SCHEMA cdm_mapping;
CREATE SCHEMA webapi;

-- TODO: add users, and other database config in this script!