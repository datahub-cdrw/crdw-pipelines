\connect test_crdw

CREATE TABLE vocab.concept (
  concept_id			INTEGER		    NOT NULL ,
  concept_name			VARCHAR(255)	NOT NULL ,
  domain_id				VARCHAR(20)		NOT NULL ,
  vocabulary_id			VARCHAR(20)		NOT NULL ,
  concept_class_id		VARCHAR(20)		NOT NULL ,
  standard_concept		VARCHAR(1)		NULL ,
  concept_code			VARCHAR(50)		NOT NULL ,
  valid_start_date		DATE		    NOT NULL ,
  valid_end_date		DATE		    NOT NULL ,
  invalid_reason		VARCHAR(1)		NULL
)
;

CREATE TABLE vocab.vocabulary (
  vocabulary_id			    VARCHAR(20)		NOT NULL,
  vocabulary_name		    VARCHAR(255)	NOT NULL,
  vocabulary_reference	    VARCHAR(255)	NOT NULL,
  vocabulary_version	    VARCHAR(255),
  vocabulary_concept_id	    INTEGER		    NOT NULL
)
;

CREATE TABLE vocab.domain (
  domain_id			    VARCHAR(20)		NOT NULL,
  domain_name		    VARCHAR(255)	NOT NULL,
  domain_concept_id	    INTEGER			NOT NULL
)
;

CREATE TABLE vocab.concept_class (
  concept_class_id			    VARCHAR(20)		NOT NULL,
  concept_class_name		    VARCHAR(255)	NOT NULL,
  concept_class_concept_id	    INTEGER			  NOT NULL
)
;

CREATE TABLE vocab.concept_relationship (
  concept_id_1			INTEGER			NOT NULL,
  concept_id_2			INTEGER			NOT NULL,
  relationship_id		VARCHAR(20)	    NOT NULL,
  valid_start_date	    DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)	    NULL
  )
;

CREATE TABLE vocab.relationship (
  relationship_id			    VARCHAR(20)		NOT NULL,
  relationship_name			    VARCHAR(255)	NOT NULL,
  is_hierarchical			    VARCHAR(1)		NOT NULL,
  defines_ancestry			    VARCHAR(1)		NOT NULL,
  reverse_relationship_id	    VARCHAR(20)		NOT NULL,
  relationship_concept_id	    INTEGER			  NOT NULL
)
;

CREATE TABLE vocab.concept_synonym (
  concept_id			      INTEGER			NOT NULL,
  concept_synonym_name	      VARCHAR(1000)	    NOT NULL,
  language_concept_id	      INTEGER			NOT NULL
)
;

CREATE TABLE vocab.concept_ancestor (
  ancestor_concept_id		    INTEGER		NOT NULL,
  descendant_concept_id		  INTEGER		NOT NULL,
  min_levels_of_separation	INTEGER		NOT NULL,
  max_levels_of_separation	INTEGER		NOT NULL
)
;

CREATE TABLE vocab.source_to_concept_map (
  source_code				      VARCHAR(50)		NOT NULL,
  source_concept_id			      INTEGER		    NOT NULL,
  source_vocabulary_id		      VARCHAR(20)		NOT NULL,
  source_code_description	      VARCHAR(255)	    NULL,
  target_concept_id			      INTEGER			NOT NULL,
  target_vocabulary_id		      VARCHAR(20)		NOT NULL,
  valid_start_date			      DATE			    NOT NULL,
  valid_end_date			      DATE			    NOT NULL,
  invalid_reason			      VARCHAR(1)		NULL
)
;

CREATE TABLE vocab.drug_strength (
  drug_concept_id				      INTEGER		  NOT NULL,
  ingredient_concept_id			      INTEGER		  NOT NULL,
  amount_value					      NUMERIC		  NULL,
  amount_unit_concept_id		      INTEGER		  NULL,
  numerator_value				      NUMERIC		  NULL,
  numerator_unit_concept_id		      INTEGER		  NULL,
  denominator_value				      NUMERIC		  NULL,
  denominator_unit_concept_id	      INTEGER		  NULL,
  box_size						      INTEGER		  NULL,
  valid_start_date				      DATE		      NOT NULL,
  valid_end_date				      DATE		      NOT NULL,
  invalid_reason				      VARCHAR(1)      NULL
)
;

CREATE TABLE vocab.cohort_definition (
  cohort_definition_id				INTEGER			NOT NULL,
  cohort_definition_name			VARCHAR(255)	NOT NULL,
  cohort_definition_description		TEXT	        NULL,
  definition_type_concept_id		INTEGER			NOT NULL,
  cohort_definition_syntax			TEXT	        NULL,
  subject_concept_id				INTEGER			NOT NULL,
  cohort_initiation_date			DATE			NULL
)
;

CREATE TABLE vocab.attribute_definition (
  attribute_definition_id		 INTEGER			  NOT NULL,
  attribute_name				 VARCHAR(255)	NOT NULL,
  attribute_description			 TEXT	NULL,
  attribute_type_concept_id		 INTEGER			  NOT NULL,
  attribute_syntax				 TEXT	NULL
)
;

-- ALTER TABLE vocab.concept ADD CONSTRAINT xpk_concept PRIMARY KEY (concept_id);

-- ALTER TABLE vocab.vocabulary ADD CONSTRAINT xpk_vocabulary PRIMARY KEY (vocabulary_id);

-- ALTER TABLE vocab.domain ADD CONSTRAINT xpk_domain PRIMARY KEY (domain_id);

-- ALTER TABLE vocab.concept_class ADD CONSTRAINT xpk_concept_class PRIMARY KEY (concept_class_id);

-- ALTER TABLE vocab.concept_relationship ADD CONSTRAINT xpk_concept_relationship PRIMARY KEY (concept_id_1,concept_id_2,relationship_id);

-- ALTER TABLE vocab.relationship ADD CONSTRAINT xpk_relationship PRIMARY KEY (relationship_id);

-- ALTER TABLE vocab.concept_ancestor ADD CONSTRAINT xpk_concept_ancestor PRIMARY KEY (ancestor_concept_id,descendant_concept_id);

-- ALTER TABLE vocab.source_to_concept_map ADD CONSTRAINT xpk_source_to_concept_map PRIMARY KEY (source_vocabulary_id,target_concept_id,source_code,valid_end_date);

-- ALTER TABLE vocab.drug_strength ADD CONSTRAINT xpk_drug_strength PRIMARY KEY (drug_concept_id, ingredient_concept_id);

-- ALTER TABLE vocab.cohort_definition ADD CONSTRAINT xpk_cohort_definition PRIMARY KEY (cohort_definition_id);

-- ALTER TABLE vocab.attribute_definition ADD CONSTRAINT xpk_attribute_definition PRIMARY KEY (attribute_definition_id);

CREATE UNIQUE INDEX idx_concept_concept_id  ON vocab.concept  (concept_id ASC);
CLUSTER vocab.concept  USING idx_concept_concept_id ;
CREATE INDEX idx_concept_code ON vocab.concept (concept_code ASC);
CREATE INDEX idx_concept_vocabluary_id ON vocab.concept (vocabulary_id ASC);
CREATE INDEX idx_concept_domain_id ON vocab.concept (domain_id ASC);
CREATE INDEX idx_concept_class_id ON vocab.concept (concept_class_id ASC);

CREATE UNIQUE INDEX idx_vocabulary_vocabulary_id  ON vocab.vocabulary  (vocabulary_id ASC);
CLUSTER vocab.vocabulary  USING idx_vocabulary_vocabulary_id ;

CREATE UNIQUE INDEX idx_domain_domain_id  ON vocab.domain  (domain_id ASC);
CLUSTER vocab.domain  USING idx_domain_domain_id ;

CREATE UNIQUE INDEX idx_concept_class_class_id  ON vocab.concept_class  (concept_class_id ASC);
CLUSTER vocab.concept_class  USING idx_concept_class_class_id ;

CREATE INDEX idx_concept_relationship_id_1 ON vocab.concept_relationship (concept_id_1 ASC);
CREATE INDEX idx_concept_relationship_id_2 ON vocab.concept_relationship (concept_id_2 ASC);
CREATE INDEX idx_concept_relationship_id_3 ON vocab.concept_relationship (relationship_id ASC);

CREATE UNIQUE INDEX idx_relationship_rel_id  ON vocab.relationship  (relationship_id ASC);
CLUSTER vocab.relationship  USING idx_relationship_rel_id ;

CREATE INDEX idx_concept_synonym_id  ON vocab.concept_synonym  (concept_id ASC);
CLUSTER vocab.concept_synonym  USING idx_concept_synonym_id ;

CREATE INDEX idx_concept_ancestor_id_1  ON vocab.concept_ancestor  (ancestor_concept_id ASC);
CLUSTER vocab.concept_ancestor  USING idx_concept_ancestor_id_1 ;
CREATE INDEX idx_concept_ancestor_id_2 ON vocab.concept_ancestor (descendant_concept_id ASC);

CREATE INDEX idx_source_to_concept_map_id_3  ON vocab.source_to_concept_map  (target_concept_id ASC);
CLUSTER vocab.source_to_concept_map  USING idx_source_to_concept_map_id_3 ;
CREATE INDEX idx_source_to_concept_map_id_1 ON vocab.source_to_concept_map (source_vocabulary_id ASC);
CREATE INDEX idx_source_to_concept_map_id_2 ON vocab.source_to_concept_map (target_vocabulary_id ASC);
CREATE INDEX idx_source_to_concept_map_code ON vocab.source_to_concept_map (source_code ASC);

CREATE INDEX idx_drug_strength_id_1  ON vocab.drug_strength  (drug_concept_id ASC);
CLUSTER vocab.drug_strength  USING idx_drug_strength_id_1 ;
CREATE INDEX idx_drug_strength_id_2 ON vocab.drug_strength (ingredient_concept_id ASC);

CREATE INDEX idx_cohort_definition_id  ON vocab.cohort_definition  (cohort_definition_id ASC);
CLUSTER vocab.cohort_definition  USING idx_cohort_definition_id ;

CREATE INDEX idx_attribute_definition_id  ON vocab.attribute_definition  (attribute_definition_id ASC);
CLUSTER vocab.attribute_definition  USING idx_attribute_definition_id ;

-- ALTER TABLE vocab.concept ADD CONSTRAINT fpk_concept_domain FOREIGN KEY (domain_id)  REFERENCES vocab.domain (domain_id);

-- ALTER TABLE vocab.concept ADD CONSTRAINT fpk_concept_class FOREIGN KEY (concept_class_id)  REFERENCES vocab.concept_class (concept_class_id);

-- ALTER TABLE vocab.concept ADD CONSTRAINT fpk_concept_vocabulary FOREIGN KEY (vocabulary_id)  REFERENCES vocab.vocabulary (vocabulary_id);

-- ALTER TABLE vocab.vocabulary ADD CONSTRAINT fpk_vocabulary_concept FOREIGN KEY (vocabulary_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.domain ADD CONSTRAINT fpk_domain_concept FOREIGN KEY (domain_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.concept_class ADD CONSTRAINT fpk_concept_class_concept FOREIGN KEY (concept_class_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.concept_relationship ADD CONSTRAINT fpk_concept_relationship_c_1 FOREIGN KEY (concept_id_1)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.concept_relationship ADD CONSTRAINT fpk_concept_relationship_c_2 FOREIGN KEY (concept_id_2)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.concept_relationship ADD CONSTRAINT fpk_concept_relationship_id FOREIGN KEY (relationship_id)  REFERENCES vocab.relationship (relationship_id);

-- ALTER TABLE vocab.relationship ADD CONSTRAINT fpk_relationship_concept FOREIGN KEY (relationship_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.relationship ADD CONSTRAINT fpk_relationship_reverse FOREIGN KEY (reverse_relationship_id)  REFERENCES vocab.relationship (relationship_id);

-- ALTER TABLE vocab.concept_synonym ADD CONSTRAINT fpk_concept_synonym_concept FOREIGN KEY (concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.concept_ancestor ADD CONSTRAINT fpk_concept_ancestor_concept_1 FOREIGN KEY (ancestor_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.concept_ancestor ADD CONSTRAINT fpk_concept_ancestor_concept_2 FOREIGN KEY (descendant_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.source_to_concept_map ADD CONSTRAINT fpk_source_to_concept_map_v_1 FOREIGN KEY (source_vocabulary_id)  REFERENCES vocab.vocabulary (vocabulary_id);

-- ALTER TABLE vocab.source_to_concept_map ADD CONSTRAINT fpk_source_to_concept_map_v_2 FOREIGN KEY (target_vocabulary_id)  REFERENCES vocab.vocabulary (vocabulary_id);

-- ALTER TABLE vocab.source_to_concept_map ADD CONSTRAINT fpk_source_to_concept_map_c_1 FOREIGN KEY (target_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.drug_strength ADD CONSTRAINT fpk_drug_strength_concept_1 FOREIGN KEY (drug_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.drug_strength ADD CONSTRAINT fpk_drug_strength_concept_2 FOREIGN KEY (ingredient_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.drug_strength ADD CONSTRAINT fpk_drug_strength_unit_1 FOREIGN KEY (amount_unit_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.drug_strength ADD CONSTRAINT fpk_drug_strength_unit_2 FOREIGN KEY (numerator_unit_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.drug_strength ADD CONSTRAINT fpk_drug_strength_unit_3 FOREIGN KEY (denominator_unit_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.cohort_definition ADD CONSTRAINT fpk_cohort_definition_concept FOREIGN KEY (definition_type_concept_id)  REFERENCES vocab.concept (concept_id);

-- ALTER TABLE vocab.concept_synonym ADD CONSTRAINT uq_concept_synonym UNIQUE (concept_id, concept_synonym_name, language_concept_id);
