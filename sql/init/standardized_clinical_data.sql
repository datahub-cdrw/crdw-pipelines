\connect test_crdw
--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.person
(
  person_id						        INTEGER	  	NOT NULL ,
  gender_concept_id				    INTEGER	  	NOT NULL ,
  year_of_birth					      INTEGER	  	NOT NULL ,
  month_of_birth				      INTEGER	  	NULL,
  day_of_birth					      INTEGER	  	NULL,
  birth_datetime				      TIMESTAMP	  NULL,
  race_concept_id				      INTEGER		  NOT NULL,
  ethnicity_concept_id			  INTEGER	  	NOT NULL,
  location_id					        INTEGER		  NULL,
  provider_id					        INTEGER		  NULL,
  care_site_id					      INTEGER		  NULL,
  person_source_value			    VARCHAR(50) NULL,
  gender_source_value			    VARCHAR(50) NULL,
  gender_source_concept_id	  INTEGER		  NULL,
  race_source_value				    VARCHAR(50) NULL,
  race_source_concept_id		  INTEGER		  NULL,
  ethnicity_source_value		  VARCHAR(50) NULL,
  ethnicity_source_concept_id	INTEGER		  NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.observation_period
(
  observation_period_id				      INTEGER		NOT NULL ,
  person_id							            INTEGER		NOT NULL ,
  observation_period_start_date		  DATE		  NOT NULL ,
  observation_period_end_date		    DATE		  NOT NULL ,
  period_type_concept_id			      INTEGER		NOT NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.specimen
(
  specimen_id						      INTEGER			NOT NULL ,
  person_id							      INTEGER			NOT NULL ,
  specimen_concept_id				  INTEGER			NOT NULL ,
  specimen_type_concept_id		INTEGER			NOT NULL ,
  specimen_date						    DATE			  NOT NULL ,
  specimen_datetime					  TIMESTAMP		NULL ,
  quantity							      NUMERIC			  NULL ,
  unit_concept_id					    INTEGER			NULL ,
  anatomic_site_concept_id		INTEGER			NULL ,
  disease_status_concept_id		INTEGER			NULL ,
  specimen_source_id				  VARCHAR(50)	NULL ,
  specimen_source_value				VARCHAR(50)	NULL ,
  unit_source_value					  VARCHAR(50)	NULL ,
  anatomic_site_source_value	VARCHAR(50)	NULL ,
  disease_status_source_value VARCHAR(50)	NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.death
(
  person_id							  INTEGER			NOT NULL ,
  death_date							DATE			  NOT NULL ,
  death_datetime					TIMESTAMP		NULL ,
  death_type_concept_id   INTEGER			NOT NULL ,
  cause_concept_id			  INTEGER			NULL ,
  cause_source_value			VARCHAR(50)	NULL,
  cause_source_concept_id INTEGER			NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.visit_occurrence
(
  visit_occurrence_id			      INTEGER			NOT NULL ,
  person_id						          INTEGER			NOT NULL ,
  visit_concept_id				      INTEGER			NOT NULL ,
  visit_start_date				      DATE			  NOT NULL ,
  visit_start_datetime				  TIMESTAMP		NULL ,
  visit_end_date					      DATE			  NOT NULL ,
  visit_end_datetime					  TIMESTAMP		NULL ,
  visit_type_concept_id			    INTEGER			NOT NULL ,
  provider_id					          INTEGER			NULL,
  care_site_id					        INTEGER			NULL,
  visit_source_value				    VARCHAR(50)	NULL,
  visit_source_concept_id		    INTEGER			NULL ,
  admitting_source_concept_id	  INTEGER			NULL ,
  admitting_source_value		    VARCHAR(50)	NULL ,
  discharge_to_concept_id		    INTEGER   	NULL ,
  discharge_to_source_value		  VARCHAR(50)	NULL ,
  preceding_visit_occurrence_id	INTEGER			NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.visit_detail
(
  visit_detail_id             INTEGER     NOT NULL ,
  person_id                   INTEGER     NOT NULL ,
  visit_detail_concept_id     INTEGER     NOT NULL ,
  visit_start_date            DATE        NOT NULL ,
  visit_start_datetime        TIMESTAMP   NULL ,
  visit_end_date              DATE        NOT NULL ,
  visit_end_datetime          TIMESTAMP   NULL ,
  visit_type_concept_id       INTEGER     NOT NULL ,
  provider_id                 INTEGER     NULL ,
  care_site_id                INTEGER     NULL ,
  admitting_source_concept_id INTEGER     NULL ,
  discharge_to_concept_id     INTEGER     NULL ,
  preceding_visit_detail_id   INTEGER     NULL ,
  visit_source_value          VARCHAR(50) NULL ,
  visit_source_concept_id     INTEGER     NULL ,
  admitting_source_value      VARCHAR(50) NULL ,
  discharge_to_source_value   VARCHAR(50) NULL ,
  visit_detail_parent_id      INTEGER     NULL ,
  visit_occurrence_id         INTEGER     NOT NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.procedure_occurrence
(
  procedure_occurrence_id		  INTEGER			NOT NULL ,
  person_id						        INTEGER			NOT NULL ,
  procedure_concept_id			  INTEGER			NOT NULL ,
  procedure_date				      DATE			  NOT NULL ,
  procedure_datetime			    TIMESTAMP		NULL ,
  procedure_type_concept_id		INTEGER			NOT NULL ,
  modifier_concept_id			    INTEGER			NULL ,
  quantity						        INTEGER			NULL ,
  provider_id					        INTEGER			NULL ,
  visit_occurrence_id			    INTEGER			NULL ,
  visit_detail_id             INTEGER     NULL ,
  procedure_source_value		  VARCHAR(50)	NULL ,
  procedure_source_concept_id	INTEGER			NULL ,
  modifier_source_value		   VARCHAR(50)	NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.drug_exposure
(
  drug_exposure_id				      INTEGER			  NOT NULL ,
  person_id						          INTEGER			  NOT NULL ,
  drug_concept_id				        INTEGER			  NOT NULL ,
  drug_exposure_start_date		  DATE			    NOT NULL ,
  drug_exposure_start_datetime  TIMESTAMP		  NULL ,
  drug_exposure_end_date		    DATE			    NOT NULL ,
  drug_exposure_end_datetime	  TIMESTAMP		  NULL ,
  verbatim_end_date				      DATE			    NULL ,
  drug_type_concept_id			    INTEGER			  NOT NULL ,
  stop_reason					          VARCHAR(20)		NULL ,
  refills						            INTEGER		  	NULL ,
  quantity						          NUMERIC			    NULL ,
  days_supply					          INTEGER		  	NULL ,
  sig							              TEXT	NULL ,
  route_concept_id				      INTEGER			  NULL ,
  lot_number					          VARCHAR(50)	  NULL ,
  provider_id					          INTEGER			  NULL ,
  visit_occurrence_id			      INTEGER			  NULL ,
  visit_detail_id               INTEGER       NULL ,
  drug_source_value				      VARCHAR(50)	  NULL ,
  drug_source_concept_id		    INTEGER			  NULL ,
  route_source_value			      VARCHAR(50)	  NULL ,
  dose_unit_source_value		    VARCHAR(50)	  NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.device_exposure
(
  device_exposure_id			        INTEGER		  	NOT NULL ,
  person_id						            INTEGER			  NOT NULL ,
  device_concept_id			        	INTEGER			  NOT NULL ,
  device_exposure_start_date	    DATE			    NOT NULL ,
  device_exposure_start_datetime  TIMESTAMP		  NULL ,
  device_exposure_end_date		    DATE			    NULL ,
  device_exposure_end_datetime    TIMESTAMP		  NULL ,
  device_type_concept_id		      INTEGER			  NOT NULL ,
  unique_device_id			        	VARCHAR(50)		NULL ,
  quantity						            INTEGER			  NULL ,
  provider_id					            INTEGER			  NULL ,
  visit_occurrence_id			        INTEGER			  NULL ,
  visit_detail_id                 INTEGER       NULL ,
  device_source_value			        VARCHAR(100)	NULL ,
  device_source_concept_id		    INTEGER			  NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.condition_occurrence
(
  condition_occurrence_id		    INTEGER			NOT NULL ,
  person_id						          INTEGER			NOT NULL ,
  condition_concept_id			    INTEGER			NOT NULL ,
  condition_start_date			    DATE			  NOT NULL ,
  condition_start_datetime		  TIMESTAMP		NULL ,
  condition_end_date			      DATE			  NULL ,
  condition_end_datetime		    TIMESTAMP		NULL ,
  condition_type_concept_id		  INTEGER			NOT NULL ,
  stop_reason					          VARCHAR(20)	NULL ,
  provider_id					          INTEGER			NULL ,
  visit_occurrence_id			      INTEGER			NULL ,
  visit_detail_id               INTEGER     NULL ,
  condition_source_value		    VARCHAR(50)	NULL ,
  condition_source_concept_id	  INTEGER			NULL ,
  condition_status_source_value	VARCHAR(50)	NULL ,
  condition_status_concept_id	  INTEGER			NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.measurement
(
  measurement_id				        INTEGER			NOT NULL ,
  person_id						          INTEGER			NOT NULL ,
  measurement_concept_id		    INTEGER			NOT NULL ,
  measurement_date				      DATE			  NOT NULL ,
  measurement_time              VARCHAR(10) NULL ,
  measurement_datetime			    TIMESTAMP		NULL ,
  measurement_type_concept_id	  INTEGER			NOT NULL ,
  operator_concept_id			      INTEGER			NULL ,
  value_as_number				        NUMERIC			  NULL ,
  value_as_concept_id			      INTEGER			NULL ,
  unit_concept_id				        INTEGER			NULL ,
  range_low					          	NUMERIC			  NULL ,
  range_high					          NUMERIC			  NULL ,
  provider_id					          INTEGER			NULL ,
  visit_occurrence_id			      INTEGER			NULL ,
  visit_detail_id               INTEGER     NULL ,
  measurement_source_value		  VARCHAR(50)	NULL ,
  measurement_source_concept_id	INTEGER			NULL ,
  unit_source_value				      VARCHAR(50)	NULL ,
  value_source_value			      VARCHAR(50)	NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.note
(
  note_id						    INTEGER			  NOT NULL ,
  person_id						  INTEGER			  NOT NULL ,
  note_date						  DATE			    NOT NULL ,
  note_datetime					TIMESTAMP		  NULL ,
  note_type_concept_id	INTEGER			  NOT NULL ,
  note_class_concept_id INTEGER			  NOT NULL ,
  note_title					  VARCHAR(250)	NULL ,
  note_text						  TEXT  NULL ,
  encoding_concept_id		INTEGER			  NOT NULL ,
  language_concept_id		INTEGER			  NOT NULL ,
  provider_id					  INTEGER			  NULL ,
  visit_occurrence_id		INTEGER			  NULL ,
  visit_detail_id       INTEGER       NULL ,
  note_source_value			VARCHAR(50)		NULL
)
;



CREATE TABLE cdm.note_nlp
(
  note_nlp_id					        INTEGER			  NOT NULL ,
  note_id						          INTEGER			  NOT NULL ,
  section_concept_id			    INTEGER			  NULL ,
  snippet						          VARCHAR(250)	NULL ,
  "offset"					          VARCHAR(250)	NULL ,
  lexical_variant				      VARCHAR(250)	NOT NULL ,
  note_nlp_concept_id			    INTEGER			  NULL ,
  note_nlp_source_concept_id  INTEGER			  NULL ,
  nlp_system					        VARCHAR(250)	NULL ,
  nlp_date						        DATE			    NOT NULL ,
  nlp_datetime					      TIMESTAMP		  NULL ,
  term_exists					        VARCHAR(1)		NULL ,
  term_temporal					      VARCHAR(50)		NULL ,
  term_modifiers				      VARCHAR(2000)	NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.observation
(
  observation_id					      INTEGER			NOT NULL ,
  person_id						          INTEGER			NOT NULL ,
  observation_concept_id			  INTEGER			NOT NULL ,
  observation_date				      DATE			  NOT NULL ,
  observation_datetime				  TIMESTAMP		NULL ,
  observation_type_concept_id	  INTEGER			NOT NULL ,
  value_as_number				        NUMERIC			  NULL ,
  value_as_string				        VARCHAR(60)	NULL ,
  value_as_concept_id			      INTEGER			NULL ,
  qualifier_concept_id			    INTEGER			NULL ,
  unit_concept_id				        INTEGER			NULL ,
  provider_id					          INTEGER			NULL ,
  visit_occurrence_id			      INTEGER			NULL ,
  visit_detail_id               INTEGER     NULL ,
  observation_source_value		  VARCHAR(50)	NULL ,
  observation_source_concept_id	INTEGER			NULL ,
  unit_source_value				      VARCHAR(50)	NULL ,
  qualifier_source_value			  VARCHAR(50)	NULL
)
;


CREATE TABLE cdm.fact_relationship
(
  domain_concept_id_1			INTEGER			NOT NULL ,
  fact_id_1						    INTEGER			NOT NULL ,
  domain_concept_id_2			INTEGER			NOT NULL ,
  fact_id_2						    INTEGER			NOT NULL ,
  relationship_concept_id	INTEGER			NOT NULL
)
;

-- ALTER TABLE cdm.person ADD CONSTRAINT xpk_person PRIMARY KEY ( person_id ) ;

-- ALTER TABLE cdm.observation_period ADD CONSTRAINT xpk_observation_period PRIMARY KEY ( observation_period_id ) ;

-- ALTER TABLE cdm.specimen ADD CONSTRAINT xpk_specimen PRIMARY KEY ( specimen_id ) ;

-- ALTER TABLE cdm.death ADD CONSTRAINT xpk_death PRIMARY KEY ( person_id ) ;

-- ALTER TABLE cdm.visit_occurrence ADD CONSTRAINT xpk_visit_occurrence PRIMARY KEY ( visit_occurrence_id ) ;

-- ALTER TABLE cdm.visit_detail ADD CONSTRAINT xpk_visit_detail PRIMARY KEY ( visit_detail_id ) ;

-- ALTER TABLE cdm.procedure_occurrence ADD CONSTRAINT xpk_procedure_occurrence PRIMARY KEY ( procedure_occurrence_id ) ;

-- ALTER TABLE cdm.drug_exposure ADD CONSTRAINT xpk_drug_exposure PRIMARY KEY ( drug_exposure_id ) ;

-- ALTER TABLE cdm.device_exposure ADD CONSTRAINT xpk_device_exposure PRIMARY KEY ( device_exposure_id ) ;

-- ALTER TABLE cdm.condition_occurrence ADD CONSTRAINT xpk_condition_occurrence PRIMARY KEY ( condition_occurrence_id ) ;

-- ALTER TABLE cdm.measurement ADD CONSTRAINT xpk_measurement PRIMARY KEY ( measurement_id ) ;

-- ALTER TABLE cdm.note ADD CONSTRAINT xpk_note PRIMARY KEY ( note_id ) ;

-- ALTER TABLE cdm.note_nlp ADD CONSTRAINT xpk_note_nlp PRIMARY KEY ( note_nlp_id ) ;

-- ALTER TABLE cdm.observation  ADD CONSTRAINT xpk_observation PRIMARY KEY ( observation_id ) ;

CREATE UNIQUE INDEX idx_person_id  ON cdm.person  (person_id ASC);
CLUSTER cdm.person  USING idx_person_id ;

CREATE INDEX idx_observation_period_id  ON cdm.observation_period  (person_id ASC);
CLUSTER cdm.observation_period  USING idx_observation_period_id ;

CREATE INDEX idx_specimen_person_id  ON cdm.specimen  (person_id ASC);
CLUSTER cdm.specimen  USING idx_specimen_person_id ;
CREATE INDEX idx_specimen_concept_id ON cdm.specimen (specimen_concept_id ASC);

CREATE INDEX idx_death_person_id  ON cdm.death  (person_id ASC);
CLUSTER cdm.death  USING idx_death_person_id ;

CREATE INDEX idx_visit_person_id  ON cdm.visit_occurrence  (person_id ASC);
CLUSTER cdm.visit_occurrence  USING idx_visit_person_id ;
CREATE INDEX idx_visit_concept_id ON cdm.visit_occurrence (visit_concept_id ASC);

CREATE INDEX idx_visit_detail_person_id  ON cdm.visit_detail  (person_id ASC);
CLUSTER cdm.visit_detail  USING idx_visit_detail_person_id ;
CREATE INDEX idx_visit_detail_concept_id ON cdm.visit_detail (visit_detail_concept_id ASC);

CREATE INDEX idx_procedure_person_id  ON cdm.procedure_occurrence  (person_id ASC);
CLUSTER cdm.procedure_occurrence  USING idx_procedure_person_id ;
CREATE INDEX idx_procedure_concept_id ON cdm.procedure_occurrence (procedure_concept_id ASC);
CREATE INDEX idx_procedure_visit_id ON cdm.procedure_occurrence (visit_occurrence_id ASC);

CREATE INDEX idx_drug_person_id  ON cdm.drug_exposure  (person_id ASC);
CLUSTER cdm.drug_exposure  USING idx_drug_person_id ;
CREATE INDEX idx_drug_concept_id ON cdm.drug_exposure (drug_concept_id ASC);
CREATE INDEX idx_drug_visit_id ON cdm.drug_exposure (visit_occurrence_id ASC);

CREATE INDEX idx_device_person_id  ON cdm.device_exposure  (person_id ASC);
CLUSTER cdm.device_exposure  USING idx_device_person_id ;
CREATE INDEX idx_device_concept_id ON cdm.device_exposure (device_concept_id ASC);
CREATE INDEX idx_device_visit_id ON cdm.device_exposure (visit_occurrence_id ASC);

CREATE INDEX idx_condition_person_id  ON cdm.condition_occurrence  (person_id ASC);
CLUSTER cdm.condition_occurrence  USING idx_condition_person_id ;
CREATE INDEX idx_condition_concept_id ON cdm.condition_occurrence (condition_concept_id ASC);
CREATE INDEX idx_condition_visit_id ON cdm.condition_occurrence (visit_occurrence_id ASC);

CREATE INDEX idx_measurement_person_id  ON cdm.measurement  (person_id ASC);
CLUSTER cdm.measurement  USING idx_measurement_person_id ;
CREATE INDEX idx_measurement_concept_id ON cdm.measurement (measurement_concept_id ASC);
CREATE INDEX idx_measurement_visit_id ON cdm.measurement (visit_occurrence_id ASC);

CREATE INDEX idx_note_person_id  ON cdm.note  (person_id ASC);
CLUSTER cdm.note  USING idx_note_person_id ;
CREATE INDEX idx_note_concept_id ON cdm.note (note_type_concept_id ASC);
CREATE INDEX idx_note_visit_id ON cdm.note (visit_occurrence_id ASC);

CREATE INDEX idx_note_nlp_note_id  ON cdm.note_nlp  (note_id ASC);
CLUSTER cdm.note_nlp  USING idx_note_nlp_note_id ;
CREATE INDEX idx_note_nlp_concept_id ON cdm.note_nlp (note_nlp_concept_id ASC);

CREATE INDEX idx_observation_person_id  ON cdm.observation  (person_id ASC);
CLUSTER cdm.observation  USING idx_observation_person_id ;
CREATE INDEX idx_observation_concept_id ON cdm.observation (observation_concept_id ASC);
CREATE INDEX idx_observation_visit_id ON cdm.observation (visit_occurrence_id ASC);

CREATE INDEX idx_fact_relationship_id_1 ON cdm.fact_relationship (domain_concept_id_1 ASC);
CREATE INDEX idx_fact_relationship_id_2 ON cdm.fact_relationship (domain_concept_id_2 ASC);
CREATE INDEX idx_fact_relationship_id_3 ON cdm.fact_relationship (relationship_concept_id ASC);
