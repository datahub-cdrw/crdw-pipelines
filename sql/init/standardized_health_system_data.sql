\connect test_crdw

CREATE TABLE cdm.location
(
  location_id					  INTEGER			  NOT NULL ,
  address_1						  VARCHAR(50)		NULL ,
  address_2						  VARCHAR(50)		NULL ,
  city							    VARCHAR(50)		NULL ,
  state							    VARCHAR(2)		NULL ,
  zip							      VARCHAR(9)		NULL ,
  county							  VARCHAR(20)		NULL ,
  location_source_value VARCHAR(50)		NULL
)
;


CREATE TABLE cdm.care_site
(
  care_site_id						      INTEGER			  NOT NULL ,
  care_site_name						    VARCHAR(255)  NULL ,
  place_of_service_concept_id	  INTEGER			  NULL ,
  location_id						        INTEGER			  NULL ,
  care_site_source_value			  VARCHAR(50)		NULL ,
  place_of_service_source_value VARCHAR(50)		NULL
)
;


CREATE TABLE cdm.provider
(
  provider_id					        INTEGER			  NOT NULL ,
  provider_name					      VARCHAR(255)	NULL ,
  NPI							            VARCHAR(20)		NULL ,
  DEA							            VARCHAR(20)		NULL ,
  specialty_concept_id			  INTEGER			  NULL ,
  care_site_id					      INTEGER			  NULL ,
  year_of_birth					      INTEGER			  NULL ,
  gender_concept_id				    INTEGER			  NULL ,
  provider_source_value			  VARCHAR(50)		NULL ,
  specialty_source_value			VARCHAR(50)		NULL ,
  specialty_source_concept_id	INTEGER			  NULL ,
  gender_source_value			    VARCHAR(50)		NULL ,
  gender_source_concept_id		INTEGER			  NULL
)
;

-- ALTER TABLE cdm.location ADD CONSTRAINT xpk_location PRIMARY KEY ( location_id ) ;

-- ALTER TABLE cdm.care_site ADD CONSTRAINT xpk_care_site PRIMARY KEY ( care_site_id ) ;

-- ALTER TABLE cdm.provider ADD CONSTRAINT xpk_provider PRIMARY KEY ( provider_id ) ;
