\connect test_crdw
--HINT DISTRIBUTE_ON_KEY(subject_id)
CREATE TABLE cdm.cohort
(
  cohort_definition_id	INTEGER		NOT NULL ,
  subject_id						INTEGER		NOT NULL ,
  cohort_start_date			DATE			NOT NULL ,
  cohort_end_date				DATE			NOT NULL
)
;


--HINT DISTRIBUTE_ON_KEY(subject_id)
CREATE TABLE cdm.cohort_attribute
(
  cohort_definition_id		INTEGER		NOT NULL ,
  subject_id						  INTEGER		NOT NULL ,
  cohort_start_date				DATE			NOT NULL ,
  cohort_end_date				  DATE			NOT NULL ,
  attribute_definition_id INTEGER		NOT NULL ,
  value_as_number				  NUMERIC			NULL ,
  value_as_concept_id			INTEGER		NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.drug_era
(
  drug_era_id					INTEGER			NOT NULL ,
  person_id						INTEGER			NOT NULL ,
  drug_concept_id			INTEGER			NOT NULL ,
  drug_era_start_date	DATE			  NOT NULL ,
  drug_era_end_date		DATE			  NOT NULL ,
  drug_exposure_count	INTEGER			NULL ,
  gap_days						INTEGER			NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.dose_era
(
  dose_era_id					  INTEGER			NOT NULL ,
  person_id						  INTEGER			NOT NULL ,
  drug_concept_id				INTEGER			NOT NULL ,
  unit_concept_id				INTEGER			NOT NULL ,
  dose_value						NUMERIC			  NOT NULL ,
  dose_era_start_date		DATE			  NOT NULL ,
  dose_era_end_date	    DATE			  NOT NULL
)
;


--HINT DISTRIBUTE_ON_KEY(person_id)
CREATE TABLE cdm.condition_era
(
  condition_era_id				    INTEGER			NOT NULL ,
  person_id						        INTEGER			NOT NULL ,
  condition_concept_id			  INTEGER			NOT NULL ,
  condition_era_start_date		DATE			  NOT NULL ,
  condition_era_end_date			DATE			  NOT NULL ,
  condition_occurrence_count	INTEGER			NULL
)
;

-- ALTER TABLE cdm.cohort ADD CONSTRAINT xpk_cohort PRIMARY KEY ( cohort_definition_id, subject_id, cohort_start_date, cohort_end_date  ) ;

-- ALTER TABLE cdm.cohort_attribute ADD CONSTRAINT xpk_cohort_attribute PRIMARY KEY ( cohort_definition_id, subject_id, cohort_start_date, cohort_end_date, attribute_definition_id ) ;

-- ALTER TABLE cdm.drug_era ADD CONSTRAINT xpk_drug_era PRIMARY KEY ( drug_era_id ) ;

-- ALTER TABLE cdm.dose_era  ADD CONSTRAINT xpk_dose_era PRIMARY KEY ( dose_era_id ) ;

-- ALTER TABLE cdm.condition_era ADD CONSTRAINT xpk_condition_era PRIMARY KEY ( condition_era_id ) ;

CREATE INDEX idx_cohort_subject_id ON cdm.cohort (subject_id ASC);
CREATE INDEX idx_cohort_c_definition_id ON cdm.cohort (cohort_definition_id ASC);

CREATE INDEX idx_ca_subject_id ON cdm.cohort_attribute (subject_id ASC);
CREATE INDEX idx_ca_definition_id ON cdm.cohort_attribute (cohort_definition_id ASC);

CREATE INDEX idx_drug_era_person_id  ON cdm.drug_era  (person_id ASC);
CLUSTER cdm.drug_era  USING idx_drug_era_person_id ;
CREATE INDEX idx_drug_era_concept_id ON cdm.drug_era (drug_concept_id ASC);

CREATE INDEX idx_dose_era_person_id  ON cdm.dose_era  (person_id ASC);
CLUSTER cdm.dose_era  USING idx_dose_era_person_id ;
CREATE INDEX idx_dose_era_concept_id ON cdm.dose_era (drug_concept_id ASC);

CREATE INDEX idx_condition_era_person_id  ON cdm.condition_era  (person_id ASC);
CLUSTER cdm.condition_era  USING idx_condition_era_person_id ;
CREATE INDEX idx_condition_era_concept_id ON cdm.condition_era (condition_concept_id ASC);