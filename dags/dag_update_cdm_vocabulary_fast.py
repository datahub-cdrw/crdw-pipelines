from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from crdw_modules.cdm.download_and_load_vocabulary import _download_cdm_csv
from crdw_modules.app_config import AppConfig
from crdw_modules.util.database_wrapper import DatabaseWrapper
import os

dag = DAG('update_cdm_vocabulary_fast',
          description='pipeline to reload vocab database fast',
          concurrency=10,
          max_active_runs=1,
          schedule_interval=None, 
          start_date=datetime(2017, 3, 20), 
          catchup=False)


def load_concept():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/concept.csv', '\t', 'vocab.concept')
    os.system('rm /tmp/concept.csv')

def load_concept_ancestor():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/concept_ancestor.csv', '\t', 'vocab.concept_ancestor')
    os.system('rm /tmp/concept_ancestor.csv')

def load_concept_class():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/concept_class.csv', '\t', 'vocab.concept_class')
    os.system('rm /tmp/concept_class.csv')

def load_concept_relationship():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/concept_relationship.csv', '\t', 'vocab.concept_relationship')
    os.system('rm /tmp/concept_relationship.csv')

def load_concept_synonym():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/concept_synonym.csv', '\t', 'vocab.concept_synonym')
    os.system('rm /tmp/concept_synonym.csv')

def load_domain():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/domain.csv', '\t', 'vocab.domain')
    os.system('rm /tmp/domain.csv')

def load_drug_strength():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/drug_strength.csv', '\t', 'vocab.drug_strength')
    os.system('rm /tmp/drug_strength.csv')

def load_relationship():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/relationship.csv', '\t', 'vocab.relationship')
    os.system('rm /tmp/relationship.csv')

def load_vocabulary():
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    database_wrapper.copy_from_csv('/tmp/vocabulary.csv', '\t', 'vocab.vocabulary')
    os.system('rm /tmp/vocabulary.csv')

task_download_concept = PythonOperator(
    task_id='task_download_concept', python_callable=_download_cdm_csv, op_kwargs={'table': 'concept'}, dag=dag)
task_load_concept = PythonOperator(
    task_id='task_load_concept', python_callable=load_concept, dag=dag)
task_download_concept_ancestor = PythonOperator(
    task_id='task_download_concept_ancestor', python_callable=_download_cdm_csv, op_kwargs={'table': 'concept_ancestor'}, dag=dag)
task_load_concept_ancestor = PythonOperator(
    task_id='task_load_concept_ancestor', python_callable=load_concept_ancestor, dag=dag)
task_download_concept_class = PythonOperator(
    task_id='task_download_concept_class', python_callable=_download_cdm_csv, op_kwargs={'table': 'concept_class'}, dag=dag)
task_load_concept_class = PythonOperator(
    task_id='task_load_concept_class', python_callable=load_concept_class, dag=dag)
task_download_concept_relationship = PythonOperator(
    task_id='task_download_concept_relationship', python_callable=_download_cdm_csv, op_kwargs={'table': 'concept_relationship'}, dag=dag)
task_load_concept_relationship = PythonOperator(
    task_id='task_load_concept_relationship', python_callable=load_concept_relationship, dag=dag)
task_download_concept_synonym = PythonOperator(
    task_id='task_download_concept_synonym', python_callable=_download_cdm_csv, op_kwargs={'table': 'concept_synonym'}, dag=dag)
task_load_concept_synonym = PythonOperator(
    task_id='task_load_concept_synonym', python_callable=load_concept_synonym, dag=dag)
task_download_domain = PythonOperator(
    task_id='task_download_domain', python_callable=_download_cdm_csv, op_kwargs={'table': 'domain'}, dag=dag)
task_load_domain = PythonOperator(
    task_id='task_load_domain', python_callable=load_domain, dag=dag)
task_download_drug_strength = PythonOperator(
    task_id='task_download_drug_strength', python_callable=_download_cdm_csv, op_kwargs={'table': 'drug_strength'}, dag=dag)
task_load_drug_strength = PythonOperator(
    task_id='task_load_drug_strength', python_callable=load_drug_strength, dag=dag)
task_download_relationship = PythonOperator(
    task_id='task_download_relationship', python_callable=_download_cdm_csv, op_kwargs={'table': 'relationship'}, dag=dag)
task_load_relationship = PythonOperator(
    task_id='task_load_relationship', python_callable=load_relationship, dag=dag)
task_download_vocabulary = PythonOperator(
    task_id='task_download_vocabulary', python_callable=_download_cdm_csv, op_kwargs={'table': 'vocabulary'}, dag=dag)
task_load_vocabulary = PythonOperator(
    task_id='task_load_vocabulary', python_callable=load_vocabulary, dag=dag)


[task_download_concept >> task_load_concept,
task_download_concept_ancestor >> task_load_concept_ancestor,
task_download_concept_class >> task_load_concept_class,
task_download_concept_relationship >> task_load_concept_relationship,
task_download_concept_synonym >> task_load_concept_synonym,
task_download_domain >> task_load_domain,
task_download_drug_strength >> task_load_drug_strength,
task_download_relationship >> task_load_relationship,
task_download_vocabulary >> task_load_vocabulary]
