from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from crdw_modules.cdm import download_and_load_vocabulary


dag = DAG('update_cdm_vocabulary',
          description='pipeline to reload vocab database',
          concurrency=10,
          max_active_runs=1,
          schedule_interval=None, 
          start_date=datetime(2017, 3, 20), 
          catchup=False)


task_load_concept = PythonOperator(
    task_id='task_load_concept', python_callable=download_and_load_vocabulary.load_concept, dag=dag)
task_load_concept_ancestor = PythonOperator(
    task_id='task_load_concept_ancestor', python_callable=download_and_load_vocabulary.load_concept_ancestor, dag=dag)
task_load_concept_class = PythonOperator(
    task_id='task_load_concept_class', python_callable=download_and_load_vocabulary.load_concept_class, dag=dag)
task_load_concept_relationship = PythonOperator(
    task_id='task_load_concept_relationship', python_callable=download_and_load_vocabulary.load_concept_relationship, dag=dag)
task_load_concept_synonym = PythonOperator(
    task_id='task_load_concept_synonym', python_callable=download_and_load_vocabulary.load_concept_synonym, dag=dag)
task_load_domain = PythonOperator(
    task_id='task_load_domain', python_callable=download_and_load_vocabulary.load_domain, dag=dag)
task_load_drug_strength = PythonOperator(
    task_id='task_load_drug_strength', python_callable=download_and_load_vocabulary.load_drug_strength, dag=dag)
task_load_relationship = PythonOperator(
    task_id='task_load_relationship', python_callable=download_and_load_vocabulary.load_relationship, dag=dag)
task_load_vocabulary = PythonOperator(
    task_id='task_load_vocabulary', python_callable=download_and_load_vocabulary.load_vocabulary, dag=dag)
# task_apply_constrains = PythonOperator(
#     task_id='task_apply_constraints', python_callable=dummy_placeholder, dag=dag)
# task_remove_constraints = PythonOperator(
#     task_id='task_remove_constraints', python_callable=dummy_placeholder, dag=dag)


task_load_concept >> task_load_concept_ancestor >> task_load_concept_class >> task_load_concept_relationship >> task_load_concept_synonym >> task_load_domain >> task_load_drug_strength >> task_load_relationship >> task_load_vocabulary 
