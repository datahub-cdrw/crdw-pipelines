DROP TABLE IF EXISTS test_table;
DROP SCHEMA IF EXISTS test_schema;

CREATE SCHEMA test_schema;

CREATE TABLE test_schema.test_table 
(
  did					INTEGER			NOT NULL PRIMARY KEY,
  date_of_birth			DATE			NULL ,
  blood_measurement	    INTEGER			NULL ,
  censored		        VARCHAR(10)			NULL 
);