DROP TABLE IF EXISTS test_table;

CREATE TABLE test_table 
(
  did					INTEGER			NOT NULL PRIMARY KEY,
  date_of_birth			DATE			NULL ,
  blood_measurement	    INTEGER			NULL ,
  censored		        VARCHAR(10)			NULL 
);