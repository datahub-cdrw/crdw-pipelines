from crdw_modules.cdm.create_crdw_db import create_crdw_db, split_data_from_vocab_tables
from crdw_modules.util.database_wrapper import DatabaseWrapper
from crdw_modules.app_config import AppConfig
import pytest

# @pytest.mark.skip(reason="do not want to clear the crdw everytime tests are run")
def test_create_new_crdw_db():
    db_config = AppConfig().get_config('vocab_database') #need to create a fresh vocab to test with
    database_wrapper = DatabaseWrapper(db_config, auto_connect=False)
    if not database_wrapper.database_exists():
        database_wrapper.create_database()
    create_crdw_db(db_name='cdm_crdw',force_reload=True)

def test_split_data_from_vocab_tables():
    split_data_from_vocab_tables()
