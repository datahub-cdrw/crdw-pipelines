from crdw_modules.cdm.create_vocab_db import create_vocab_db, split_vocab_from_data_tables
from crdw_modules.app_config import AppConfig
import pytest


# @pytest.mark.skip(reason="do not want to clear the vocab everytime tests are run")
def test_create_vocab_db():
    create_vocab_db(force_reload=True)


def test_split_vocab_from_data_tables():
    split_vocab_from_data_tables()

