from crdw_modules.cdm.checkout_cdm_version import checkout_cdm_version
from crdw_modules.app_config import AppConfig
import os


def test_create_vocab_db():
    checkout_cdm_version()
    assert(os.path.isdir('{}/cdm/PostgreSQL'.format(os.environ['AIRFLOW_HOME'])))
    assert(os.path.isfile('{}/cdm_sql/create_vocab.sql'.format(os.environ['AIRFLOW_HOME'])))