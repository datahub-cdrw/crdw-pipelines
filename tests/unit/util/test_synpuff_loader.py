from crdw_modules.util.synpuff_loader import synpuff_loader, _download_and_unzip, _insert_csv_into_table
from crdw_modules.util.database_wrapper import DatabaseWrapper
from crdw_modules.cdm.create_new_datamart import create_new_datamart
from crdw_modules.app_config import AppConfig
import pytest, os, logging

@pytest.mark.skip(reason='this is an integration test')
def test_synpuff_loader():
    synpuff_loader()

def test__download_and_unzip():
    _download_and_unzip()
    logging.debug(os.listdir('/tmp/synpuff'))
    assert os.path.isfile('/tmp/synpuff/person.csv')

def test__insert_csv_into_table():
    _download_and_unzip()
    create_new_datamart('dm_synpuff_test', force_reload=True)
    database_wrapper = DatabaseWrapper(AppConfig().get_config('crdw_database'))
    _insert_csv_into_table(database_wrapper, 'person.csv', schema='dm_synpuff_test')
    database_wrapper.execute_query('DROP SCHEMA dm_synpuff_test CASCADE')
    