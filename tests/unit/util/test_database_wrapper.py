from crdw_modules.util.database_wrapper import DatabaseWrapper
from typing import Dict
import pytest, uuid, logging, json


## ------------------------ generic stuff -------------------------------------
TEST_CSV_FILE = '/usr/local/crdw/tests/data/test.csv'
TEST_CSV_FILE_INVALID = '/usr/local/crdw/tests/data/test_invalid.csv'

def generate_test_config() -> Dict[str,str]:
    config = dict()
    config['host'] = 'test_postgres'
    config['port'] = '5432'
    config['database'] = 'postgres'
    config['user'] = 'postgres'
    config['password'] = 'postgres'
    return config


## ------------------------ functionality tests -------------------------------


def test_connection():
    database_wrapper = DatabaseWrapper(generate_test_config())
    assert database_wrapper.is_connected()
    database_wrapper.close_database_connection()

def test_no_auto_connection():
    database_wrapper = DatabaseWrapper(generate_test_config(), auto_connect=False)
    if not database_wrapper.is_connected():
        database_wrapper.open_database_connection()
        assert database_wrapper.is_connected()
    database_wrapper.close_database_connection()

def test_execute_query():
    database_wrapper = DatabaseWrapper(generate_test_config())
    result = database_wrapper.execute_select_query('SELECT datname FROM pg_database')
    assert 'postgres' in result.__str__()
    database_wrapper.close_database_connection()

def test_execute_select_query_from_file():
    database_wrapper = DatabaseWrapper(generate_test_config())
    database_wrapper.execute_select_query_from_file('/usr/local/crdw/tests/sql/test1.sql')
    database_wrapper.close_database_connection()

def test_create_database():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    database_wrapper = DatabaseWrapper(config, auto_connect=False)
    database_wrapper.create_database()
    database_wrapper.close_database_connection()
    assert database_wrapper.database_exists()
    database_wrapper.drop_database()
    assert not database_wrapper.database_exists()

def test_create_schema():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    database_wrapper.create_schema('test')
    assert database_wrapper.schema_exists('test')
    database_wrapper.create_schema('test')
    database_wrapper.drop_schema('test')
    assert not database_wrapper.schema_exists('test')
    database_wrapper.drop_schema('test')

def test_check_database_exists():
    none_config = generate_test_config()
    none_config['database'] = uuid.uuid1().__str__()
    none_database_wrapper = DatabaseWrapper(none_config, auto_connect=False)
    assert not none_database_wrapper.database_exists()
    config = generate_test_config()
    config['database'] = 'postgres'
    database_wrapper = DatabaseWrapper(config, auto_connect=True)
    assert database_wrapper.database_exists()

def test_auto_create_database_on_conncetion():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    assert database_wrapper.database_exists()
    database_wrapper.drop_database()

def test_create_database_connection_wrong_db():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    with pytest.raises(ValueError, match='.*does not exist.*'):
        DatabaseWrapper(config, auto_connect=True)

def test_copy_from_csv():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    database_wrapper.execute_query_from_file('/usr/local/crdw/tests/sql/test_create.sql')
    database_wrapper.copy_from_csv(TEST_CSV_FILE, '\t', 'test_table')
    result = database_wrapper.execute_select_query('SELECT * FROM test_table;')
    assert 13 == result[0][2]
    database_wrapper.drop_database()

def test_copy_from_csv_to_specific_schema():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    database_wrapper.execute_query_from_file('/usr/local/crdw/tests/sql/test_create_with_schema.sql')
    database_wrapper.copy_from_csv(TEST_CSV_FILE, '\t', 'test_schema.test_table')
    result = database_wrapper.execute_select_query('SELECT * FROM test_schema.test_table;')
    assert 13 == result[0][2]
    database_wrapper.drop_database()

def test_copy_from_csv_tmp_table():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    database_wrapper.execute_query_from_file('/usr/local/crdw/tests/sql/test_create.sql')
    
    database_columns = ['did', 'date_of_birth', 'blood_measurement', 'censored']
    csv_header = ['did','dob','hb','censor']
    destination_types = ['INTEGER', 'DATE', 'INTEGER', 'INTEGER']
    primary_key = 'did'
    where_constraints = 'WHERE did is not NULL'

    database_wrapper.copy_from_csv_tmp_table(TEST_CSV_FILE, '\t', 'test_table', database_columns, csv_header, destination_types, primary_key, where_constraints)
    result = database_wrapper.execute_select_query('SELECT * FROM test_table;')
    assert 13 == result[0][2]
    database_wrapper.drop_database()

def test_copy_from_csv_tmp_table_constraint_violation():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    database_wrapper.execute_query_from_file('/usr/local/crdw/tests/sql/test_create.sql')
    
    database_columns = ['did', 'date_of_birth', 'blood_measurement', 'censored']
    csv_header = ['did','dob','hb','censor']
    destination_types = ['INTEGER', 'DATE', 'INTEGER', 'INTEGER']
    primary_key = 'did'
    where_constraints = 'WHERE did is not NULL'

    database_wrapper.copy_from_csv_tmp_table(TEST_CSV_FILE_INVALID, '\t', 'test_table', database_columns, csv_header, destination_types, primary_key, where_constraints)
    result = database_wrapper.execute_select_query('SELECT * FROM test_table;')
    logging.debug(result)
    assert 13 == result[0][2]
    database_wrapper.drop_database()

## ------------------------ exception tests -----------------------------------
def test_create_database_connection_open_error():
    database_wrapper = DatabaseWrapper(generate_test_config())
    with pytest.raises(RuntimeError, match='.*already connected.*'):
        database_wrapper.create_database()
    database_wrapper.close_database_connection()

def test_invalid_config():
    with pytest.raises(ValueError, match='.*dict.*'):
        DatabaseWrapper('INVALID CONFIG')

def test_copy_from_csv_constraint_violation():
    config = generate_test_config()
    config['database'] = uuid.uuid1().__str__()
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    database_wrapper.execute_query_from_file('/usr/local/crdw/tests/sql/test_create.sql')
    with pytest.raises(Exception, match='.*constraint.*'):
        database_wrapper.copy_from_csv(TEST_CSV_FILE_INVALID, '\t', 'test_table')
    database_wrapper.drop_database()