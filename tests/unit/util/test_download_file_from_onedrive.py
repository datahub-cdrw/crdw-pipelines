from crdw_modules.util.download_file_from_onedrive import download_file_from_onedrive
from typing import Dict
import pytest, uuid, os


def test_download_file_from_onedrive():
    os.system('rm /tmp/test.csv')
    download_file_from_onedrive('https://1drv.ms/u/s!AsqLiBm4pNyQgZ4xPD6T50G18kS54A?e=6XXkDp', '/tmp/test.csv')
    assert(os.path.isfile('/tmp/test.csv'))