from crdw_modules.cdm.download_and_load_vocabulary import _download_cdm_csv, _load_csv_to_database
from crdw_modules.util.database_wrapper import DatabaseWrapper
from crdw_modules.app_config import AppConfig
import os, pytest


    
def test_download_and_load_vocabulary():
    vocab_config = AppConfig().get_config('vocab_database')
    database_wrapper = DatabaseWrapper(vocab_config)
    # database_wrapper.execute_query('DROP SCHEMA test CASCADE')
    database_wrapper.create_schema('test')
    database_wrapper.execute_query(
        '''CREATE TABLE test.domain (
        domain_id			    VARCHAR(20)		NOT NULL,
        domain_name		    VARCHAR(255)	NOT NULL,
        domain_concept_id	    INTEGER			NOT NULL
        )
        ;''')
    database_wrapper.execute_query('ALTER TABLE test.domain ADD CONSTRAINT xpk_domain PRIMARY KEY (domain_id);')
    _download_cdm_csv('DOMAIN')
    insert_query = '''
        INSERT INTO test.domain(domain_id, domain_name, domain_concept_id)
        SELECT * FROM test.domain_tmp WHERE domain_id IS NOT NULL 
                                        AND domain_name IS NOT NULL 
                                        AND domain_concept_id IS NOT NULL
        ON CONFLICT (domain_id) DO NOTHING
    '''
    _load_csv_to_database(database_wrapper, 'domain', 'test', insert_query)
    result = database_wrapper.execute_select_query('SELECT * FROM test.DOMAIN')
    database_wrapper.execute_query('DROP SCHEMA test CASCADE')
    assert result.__len__() > 0

@pytest.mark.skip(reason="old code i want to keep")
def test__download_and_load_vocabulary_old():
    os.system('rm /tmp/domain.csv')    
    vocab_config = AppConfig().get_config('vocab_database')
    db_wrapper = DatabaseWrapper(vocab_config)
    db_wrapper.execute_query('DROP SCHEMA test CASCADE')
    db_wrapper.create_schema('test')
    db_wrapper.execute_query(
        '''CREATE TABLE test.domain (
        domain_id			    VARCHAR(20)		NOT NULL,
        domain_name		    VARCHAR(255)	NOT NULL,
        domain_concept_id	    INTEGER			NOT NULL
        )
        ;''')
    
    table_name = 'DOMAIN'
    database_columns = ['domain_id', 'domain_name', 'domain_concept_id']
    destination_types = ['VARCHAR(20)', 'VARCHAR(255)', 'INTEGER']
    where_constraints = 'WHERE domain_id is not null and domain_name is not null and domain_concept_id is not null'
    # _download_and_load_vocabulary(table_name, 'test', database_columns, database_columns, destination_types, None, where_constraints)

    result = db_wrapper.execute_select_query('SELECT * FROM test.DOMAIN')
    db_wrapper.execute_query('DROP SCHEMA test CASCADE')
    assert os.path.isfile('/tmp/domain.csv')
    assert result.__len__() > 0