from crdw_modules.cdm.create_new_datamart import create_new_datamart
from crdw_modules.util.database_wrapper import DatabaseWrapper
from crdw_modules.app_config import AppConfig
import logging


def test_create_new_datamart():
    vocab_config = AppConfig().get_config('vocab_database')
    db_wrapper = DatabaseWrapper(vocab_config)
    create_new_datamart('dm_test')
    result = db_wrapper.execute_select_query('''
        SELECT DISTINCT table_name
        FROM information_schema.columns 
        WHERE table_schema = 'dm_test'
    ''')
    db_wrapper.execute_query('DROP SCHEMA dm_test CASCADE')
    assert result.__len__() > 7 #random
    logging.debug(result)