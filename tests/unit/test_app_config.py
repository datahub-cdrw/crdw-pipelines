from crdw_modules.app_config import AppConfig
import pytest


def test_database_config():
    db_config = AppConfig().get_config('vocab_database')
    assert 'test_postgres' == db_config['host']

def test_cdm_version_config():
    cdm_config = AppConfig().get_config('cdm_version_config')
    assert 'https://github.com/OHDSI/CommonDataModel.git' == cdm_config['url']

def test_is_valid_key():
    app_config = AppConfig()
    assert True == app_config.is_valid_key('vocab_database')
    assert False == app_config.is_valid_key('invalid_key')

def test_missing_config():
    with pytest.raises(ValueError, match='.*config not found.*'):
        AppConfig(mode='development', file_location='/usr/local/crdw/tests/config.ini').get_config('invalid_key')
    
def test_invalid_config():
    with pytest.raises(FileNotFoundError):
        AppConfig(mode='development', file_location='INVALID_LOCATION')
