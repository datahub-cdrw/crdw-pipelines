cd /tmp/synthea
# Download the synthea jar

FILE=/tmp/synthea/synthea-with-dependencies.jar
if [[ -f "$FILE" ]]; then
    rm /tmp/synthea/synthea-with-dependencies.jar
fi

wget https://github.com/synthetichealth/synthea/releases/download/master-branch-latest/synthea-with-dependencies.jar

# Run synthea to create 100 persons
java -jar /tmp/synthea/synthea-with-dependencies.jar -c /tmp/synthea/synthea.properties -p 100 

# Run the ETL to load the synthea set into OMOP 
Rscript /tmp/synthea/ETL_synthea.r