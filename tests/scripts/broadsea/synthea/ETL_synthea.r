devtools::install_github("chrisroederucdenver/ETL-Synthea-1" , ref="v5.3.1-updates-maintenance")

library(ETLSyntheaBuilder)
 
cd <- DatabaseConnector::createConnectionDetails(
  dbms     = "postgresql", 
  server   = "test_postgres/test_crdw", 
  user     = "postgres", 
  password = "postgres", 
  port     = 5432
)

ETLSyntheaBuilder::DropVocabTables(cd,"dm_synthea10")
ETLSyntheaBuilder::DropEventTables(cd,"dm_synthea10")
ETLSyntheaBuilder::DropSyntheaTables(cd,"staging_dm_synthea10")
ETLSyntheaBuilder::CreateVocabTables(cd,"dm_synthea10")
ETLSyntheaBuilder::CreateEventTables(cd,"dm_synthea10")
ETLSyntheaBuilder::CreateSyntheaTables(cd,"staging_dm_synthea10")
ETLSyntheaBuilder::LoadSyntheaTables(cd,"staging_dm_synthea10","/tmp/synthea/output/csv")
ETLSyntheaBuilder::LoadVocabFromSchema(cd,"vocab","dm_synthea10")
ETLSyntheaBuilder::CreateVocabMapTables(cd,"dm_synthea10")
ETLSyntheaBuilder::CreateVisitRollupTables(cd,"dm_synthea10","staging_dm_synthea10")
ETLSyntheaBuilder::LoadEventTables(cd,"dm_synthea10","staging_dm_synthea10")
ETLSyntheaBuilder::LoadEventTables(cd,"dm_synthea10","staging_dm_synthea10")
ETLSyntheaBuilder::DropVocabTables(cd,"dm_synthea10")