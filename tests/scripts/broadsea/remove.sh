if [ "$(docker ps -q -f name=test_broadsea)" ]; then
    docker stop test_broadsea
    docker rm test_broadsea
    docker stop test_broadsea-methodslibrary
    docker rm test_broadsea-methodslibrary
    docker network prune --force
    docker volume prune --force
fi