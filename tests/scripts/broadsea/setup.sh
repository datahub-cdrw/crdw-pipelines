if [ ! "$(docker ps -q -f name=test_broadsea)" ]; then
    docker pull ohdsi/broadsea-webtools
    docker run -d -p 8081:8080 \
        --env WEBAPI_URL="http://localhost:8080" \
        --env datasource_driverClassName=org.postgresql.Driver \
        --env datasource_url="jdbc:postgresql://test_postgres:5432/test_crdw" \
        --env datasource_username=postgres \
        --env datasource_password=postgres \
        --env datasource.cdm.schema=dm_synpuff \
        --env datasource.ohdsi.schema=webapi \
        --env spring.jpa.properties.hibernate.default_schema=webapi \
        --env spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect \
        --env spring.batch.repository.tableprefix=webapi.BATCH_ \
        --env flyway_datasource_driverClassName=org.postgresql.Driver \
        --env flyway_datasource_url="jdbc:postgresql://test_postgres:5432/test_crdw" \
        --env flyway_schemas=webapi \
        --env flyway.placeholders.ohdsiSchema=webapi \
        --env flyway_datasource_username=postgres \
        --env flyway_datasource_password=postgres \
        --env flyway.locations=classpath:db/migration/postgresql \
        --mount type=bind,source=$(pwd)/tests/scripts/broadsea/config-local.js,target=/usr/local/tomcat/webapps/atlas/js/config-local.js \
        --network=n_test \
        --name test_broadsea \
        ohdsi/broadsea-webtools

fi

if [ ! "$(docker ps -q -f name=test_broadsea-methodslibrary)" ]; then
    docker pull ohdsi/broadsea-methodslibrary
    docker run -d -p 8787:8787 \
        -p 6311:6311 \
        --env PASSWORD="mypass" \
        --network=n_test \
        --name test_broadsea-methodslibrary \
        --mount type=bind,source=$(pwd)/tests/scripts/broadsea/run_achilles.r,target=/tmp/run_achilles.r \
        --mount type=bind,source=$(pwd)/tests/scripts/broadsea/synthea,target=/tmp/synthea \
        ohdsi/broadsea-methodslibrary
fi

# We need to update Achilles to the newest version
docker exec -it test_broadsea-methodslibrary R -e 'devtools::install_github("OHDSI/Achilles")'

# We need to stop broadsea to add the source daimon sql
docker stop test_broadsea

# run the synthea generation and ETL 
docker exec -it test_broadsea-methodslibrary sh ./tmp/synthea/run_synthea.sh

#Copy the broadsea source daimon sql to postgress container and run it
docker cp $(pwd)/tests/scripts/broadsea/broadsea_source_daimon.sql test_postgres:/tmp/broadsea_source_daimon.sql
docker exec -it test_postgres psql --echo-queries "postgresql://postgres:postgres@localhost:5432/test_crdw" -f "/tmp/broadsea_source_daimon.sql"

#Copy the results schema for dm_synpuff sql to postgress container and run it
docker cp $(pwd)/tests/scripts/broadsea/broadsea_create_dm_synpuff_results.sql test_postgres:/tmp/broadsea_create_dm_synpuff_results.sql
docker exec -it test_postgres psql --echo-queries "postgresql://postgres:postgres@localhost:5432/test_crdw" -f "/tmp/broadsea_create_dm_synpuff_results.sql"

#Copy the results schema for cdm sql to postgress container and run it
docker cp $(pwd)/tests/scripts/broadsea/broadsea_create_cdm_results.sql test_postgres:/tmp/broadsea_create_cdm_results.sql
docker exec -it test_postgres psql --echo-queries "postgresql://postgres:postgres@localhost:5432/test_crdw" -f "/tmp/broadsea_create_cdm_results.sql"

#Copy the results schema for synthea sql to postgress container and run it
docker cp $(pwd)/tests/scripts/broadsea/broadsea_create_dm_synthea_results.sql test_postgres:/tmp/broadsea_create_dm_synthea_results.sql
docker exec -it test_postgres psql --echo-queries "postgresql://postgres:postgres@localhost:5432/test_crdw" -f "/tmp/broadsea_create_dm_synthea_results.sql"

# Run Achilles
docker exec -it test_broadsea-methodslibrary Rscript /tmp/run_achilles.r

# Start the broadsea container again
docker start test_broadsea

# https://github.com/OHDSI/WebAPI/wiki/CDM-Configuration
# https://github.com/OHDSI/WebAPI/wiki/WebAPI-Installation-Guide
# https://github.com/OHDSI/Achilles

