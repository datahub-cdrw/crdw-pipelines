library(Achilles)

connectionDetails <- createConnectionDetails(
  dbms="postgresql",
  server="test_postgres/test_crdw",
  user="postgres",
  password='postgres',
  port="5432")

# Default CDM
achilles(connectionDetails,
  cdmDatabaseSchema = "cdm",
  resultsDatabaseSchema="cdm_results",
  vocabDatabaseSchema = "vocab",
  numThreads = 1,
  sourceName = "CDM",
  cdmVersion = "5.3.1",
  runHeel = TRUE,
  runCostAnalysis = TRUE,
  sqlOnly = FALSE,
  createIndices = TRUE)

# Synpuf1k
achilles(connectionDetails,
  cdmDatabaseSchema = "dm_synpuff",
  resultsDatabaseSchema="dm_synpuff_results",
  vocabDatabaseSchema = "vocab",
  numThreads = 1,
  sourceName = "synpuf1k",
  cdmVersion = "5.3.1",
  runHeel = TRUE,
  runCostAnalysis = TRUE,
  sqlOnly = FALSE,
  createIndices = TRUE)

# Synthea
achilles(connectionDetails,
  cdmDatabaseSchema = "dm_synthea10",
  resultsDatabaseSchema="dm_synthea10_results",
  vocabDatabaseSchema = "vocab",
  numThreads = 1,
  sourceName = "dm_synthea10",
  cdmVersion = "5.3.1",
  runHeel = TRUE,
  runCostAnalysis = TRUE,
  sqlOnly = FALSE,
  createIndices = TRUE)
