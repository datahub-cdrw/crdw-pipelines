\connect test_crdw

-- Default CDM --
INSERT INTO webapi.source( source_id, source_name, source_key, source_connection, source_dialect)
VALUES (1, 'CDM', 'CDM', 'jdbc:postgresql://test_postgres:5432/test_crdw?user=postgres&password=postgres', 'postgresql');

-- CDM daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (1, 1, 0, 'cdm', 2);

-- VOCABULARY daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (2, 1, 1, 'vocab', 2);

-- RESULTS daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (3, 1, 2, 'cdm_results', 2);

-- EVIDENCE daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (4, 1, 3, 'cdm_results', 2);
-- END Default CDM --


-- Synpuf --
INSERT INTO webapi.source( source_id, source_name, source_key, source_connection, source_dialect)
VALUES (2, 'synpuf1k', 'Synpuf1k', 'jdbc:postgresql://test_postgres:5432/test_crdw?user=postgres&password=postgres', 'postgresql');

-- CDM daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (5, 2, 0, 'dm_synpuff', 1);

-- VOCABULARY daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (6, 2, 1, 'vocab', 1);

-- RESULTS daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (7, 2, 2, 'dm_synpuff_results', 1);

-- EVIDENCE daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (8, 2, 3, 'dm_synpuff_results', 1);
-- END Synpuf --

-- synthea10 --
INSERT INTO webapi.source( source_id, source_name, source_key, source_connection, source_dialect)
VALUES (3, 'synthea', 'synthea', 'jdbc:postgresql://test_postgres:5432/test_crdw?user=postgres&password=postgres', 'postgresql');

-- CDM daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (9, 3, 0, 'dm_synthea10', 1);

-- VOCABULARY daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (10, 3, 1, 'vocab', 1);

-- RESULTS daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (11, 3, 2, 'dm_synthea10_results', 1);

-- EVIDENCE daimon
INSERT INTO webapi.source_daimon( source_daimon_id, source_id, daimon_type, table_qualifier, priority) VALUES (12, 3, 3, 'dm_synthea10_results', 1);
-- END synthea10 --
