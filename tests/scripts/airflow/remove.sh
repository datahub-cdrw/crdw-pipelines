if [ "$(docker ps -q -f name=test_airflow)" ]; then
    docker stop test_airflow
    docker rm test_airflow
    docker network prune --force
    docker volume prune --force
fi