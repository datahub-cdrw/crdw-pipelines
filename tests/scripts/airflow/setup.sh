if [ ! "$(docker ps -q -f name=test_airflow)" ]; then
    # run your container
    docker build --no-cache -t test_airflow -f ./tests/Dockerfile .
    docker run -d -p 8080:8080\
        --mount type=bind,source=$(pwd),target=/usr/local/crdw \
        --mount type=bind,source=$(pwd)/dags,target=/usr/local/airflow/dags \
        --network=n_test \
        --name test_airflow \
        test_airflow
    
    sleep 5
    docker exec -u airflow  test_airflow pip install /usr/local/crdw
    docker restart test_airflow
fi