if [ "$(docker ps -q -f name=test_mock_mapping)" ]; then
    docker stop test_mock_mapping
    docker rm test_mock_mapping
    docker stop test_mock_mpi
    docker rm test_mock_mpi
    docker network prune --force
    docker volume prune --force
fi
