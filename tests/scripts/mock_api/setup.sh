if [ ! "$(docker ps -q -f name=test_mock_mapping)" ]; then
    docker build -t test_json_server -f ./tests/data/json_server/Dockerfile .
    docker run -d -p 8888:80\
        --mount type=bind,source=$(pwd)/tests/data/json_server/mapping,target=/opt/json \
        --network=n_test \
        --name test_mock_mapping \
        test_json_server

    docker build -t test_mock_mpi -f ./tests/data/json_server/Dockerfile .
    docker run -d -p 9999:80\
        --mount type=bind,source=$(pwd)/tests/data/json_server/mpi,target=/opt/json \
        --network=n_test \
        --name test_mock_mpi \
        test_json_server
fi
