if [ "$(docker ps -q -f name=test_vocab)" ]; then
    docker stop test_vocab
    docker rm test_vocab
    docker network prune --force
    docker volume prune --force
fi