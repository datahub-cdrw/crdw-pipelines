if [ ! "$(docker ps -q -f name=test_vocab)" ]; then
    docker volume create v_test_vocab
    docker pull postgres
    docker run -d \
        --env POSTGRES_USER=postgres \
        --env POSTGRES_PASSWORD=postgres \
        --mount type=volume,source=v_test_vocab,target=/var/lib/postgresql/data \
        --network=n_test \
        --name test_vocab \
        postgres
fi