if [ ! "$(docker ps -q -f name=test_postgres)" ]; then
    docker pull postgres:10
    docker volume create v_test_postgres
    docker run -d \
        --env POSTGRES_USER=postgres \
        --env POSTGRES_PASSWORD=postgres \
        --mount type=volume,source=v_test_postgres,target=/var/lib/postgresql/data \
        --mount type=bind,source=$(pwd)/sql/init/,target=/docker-entrypoint-initdb.d/ \
        --network=n_test \
        --name test_postgres \
        postgres:10

    docker pull dpage/pgadmin4
    docker run -d -p 5555:80 \
        --mount type=bind,source=$(pwd)/tests/scripts/postgres/servers.json,target=/pgadmin4/servers.json \
        --env PGADMIN_DEFAULT_EMAIL=admin@datahub.nl \
        --env PGADMIN_DEFAULT_PASSWORD=admin \
        --network=n_test \
        --name test_pgadmin \
        dpage/pgadmin4
fi