if [ "$(docker ps -q -f name=test_postgres)" ]; then
    docker stop test_postgres
    docker rm test_postgres
    docker stop test_pgadmin
    docker rm test_pgadmin
    docker network prune --force
    docker volume prune --force
fi