docker network create n_test
sh ./tests/scripts/postgres/setup.sh
if test "$1" = "unit"; then
    sh ./tests/scripts/python/setup.sh
else
    sh ./tests/scripts/airflow/setup.sh
fi

if test "$1" = "broadsea" || test "$2" = "broadsea"; then
    sh ./tests/scripts/broadsea/setup.sh
fi

#depracated?
if test "$2" = "split"; then
    sh ./tests/scripts/vocab/setup.sh
fi