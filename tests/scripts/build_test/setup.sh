docker network create n_build

docker pull postgres:10
docker volume create v_build_postgres
docker run -d \
    --env POSTGRES_USER=postgres \
    --env POSTGRES_PASSWORD=postgres \
    --mount type=volume,source=v_build_postgres,target=/var/lib/postgresql/data \
    --mount type=bind,source=$(pwd)/sql/init/,target=/docker-entrypoint-initdb.d/ \
    --network=n_build \
    --name build_postgres \
    postgres:10

docker run -td \
    --mount type=bind,source=$(pwd),target=/usr/local/crdw \
    --env CRDW_INI_LOCATION=/usr/local/crdw/tests/config.ini \
    --network=n_build \
    --name build_python \
    python:3.7

docker exec build_python pip install --upgrade pip
docker exec build_python pip install pytest
docker exec build_python pip install /usr/local/crdw