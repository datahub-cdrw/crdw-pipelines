sh ./tests/scripts/postgres/remove.sh
if [ "$(docker ps -q -f name=test_python)" ]; then
    sh ./tests/scripts/python/remove.sh
fi
if [ "$(docker ps -q -f name=test_airflow)" ]; then
    sh ./tests/scripts/airflow/remove.sh
fi
if [ "$(docker ps -q -f name=test_vocab)" ]; then
    sh ./tests/scripts/vocab/remove.sh
fi
if [ "$(docker ps -q -f name=test_broadsea)" ]; then
    sh ./tests/scripts/broadsea/remove.sh
fi