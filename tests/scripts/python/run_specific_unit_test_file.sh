clear
docker exec test_python pip install -q /usr/local/crdw/
docker exec test_python python -m pytest /usr/local/crdw/tests/unit/$1 -s --log-cli-level=DEBUG
# docker exec -u airflow test_pipelines python -m pytest /usr/local/crdw/tests/unit/$1 -s --log-cli-level=DEBUG