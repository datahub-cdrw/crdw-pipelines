if [ "$(docker ps -q -f name=test_python)" ]; then
    docker stop test_python
    docker rm test_python
    docker network prune --force
    docker volume prune --force
fi