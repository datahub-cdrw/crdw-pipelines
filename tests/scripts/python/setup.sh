if [ ! "$(docker ps -q -f name=test_python)" ]; then
    docker run -td \
        --mount type=bind,source=$(pwd),target=/usr/local/crdw \
        --env CRDW_INI_LOCATION=/usr/local/crdw/tests/config.ini \
        --network=n_test \
        --name test_python \
        python:3.7

    docker exec test_python pip install --upgrade pip
    docker exec test_python pip install pytest
    docker exec test_python pip install -q /usr/local/crdw
fi