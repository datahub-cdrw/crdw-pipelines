from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from crdw_modules.app_config import AppConfig
from crdw_modules import initialize


dag = DAG('environment_build', description='pipeline to setup crdw database(s)',
          schedule_interval=None, start_date=datetime(2017, 3, 20), catchup=False)

task_initialize_vocab = PythonOperator(
    task_id='task_initialize_vocab', python_callable=initialize.initialize_vocab(), dag=dag)

task_initialize_staging = PythonOperator(
    task_id='task_initialize_staging', python_callable=initialize.initialize_staging(), dag=dag)

task_initialize_mapping = PythonOperator(
    task_id='task_initialize_mapping', python_callable=initialize.initialize_mapping(), dag=dag)

task_initialize_atlas = PythonOperator(
    task_id='task_initialize_atlas', python_callable=initialize.initialize_atlas(), dag=dag)

task_initialize_cdm = PythonOperator(
    task_id='task_initialize_cdm', python_callable=initialize.initialize_cdm(), dag=dag)

task_initialize_vocab >> [
    task_initialize_staging, 
    task_initialize_mapping, 
    task_initialize_atlas, 
    task_initialize_cdm]
