from crdw_modules.app_config import AppConfig
from crdw_modules.util.database_wrapper import DatabaseWrapper
import os
import logging

VOCAB_IDENTIFIER = '/************************\n\nStandardized vocabulary\n\n************************/'
AFTER_VOCAB_IDENTIFIER = '/**************************\n\nStandardized meta-data\n\n***************************/'


def create_vocab_db(force_reload: bool = True, schema:str = 'crdw'):
    db_config = AppConfig().get_config('vocab_database')

    db_wrapper = DatabaseWrapper(db_config, auto_connect=False)
    if force_reload and db_wrapper.database_exists():
        db_wrapper.drop_database()
    db_wrapper.create_database()
    db_wrapper.execute_query('CREATE SCHEMA IF NOT EXISTS {}; {}'.format(schema, split_vocab_from_data_tables()))
    db_wrapper.close_database_connection()
    logging.info('succesfully created the vocab database')

def split_vocab_from_data_tables() -> str:
    sql_string = open('{}/cdm_sql/create_vocab.sql'.format(os.environ['AIRFLOW_HOME']), 'r').read()
    sql_split_string = sql_string.split(VOCAB_IDENTIFIER)
    vocab_string = sql_split_string[1].split(AFTER_VOCAB_IDENTIFIER)
    logging.debug('sql_split resulted in: {}'.format(vocab_string[0]))
    return vocab_string[0]