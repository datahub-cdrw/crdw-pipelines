import logging 
import os
import subprocess
from typing import List, Dict
from crdw_modules.app_config import AppConfig


def checkout_cdm_version() -> List:
    cdm_config = AppConfig().get_config('cdm_version_config')
    git_clone_configured_version(cdm_config)
    prepare_queries(cdm_config)


def git_clone_configured_version(cdm_config: Dict):
    try:
        if os.path.isdir('{}/cdm'.format(os.environ['AIRFLOW_HOME'])):
            logging.info('path exists, cleaning up and starting from scratch')
            subprocess.check_output(['rm -rf \"$AIRFLOW_HOME/cdm\"'], shell=True)
        subprocess.check_output(['mkdir \"$AIRFLOW_HOME/cdm\"'], shell=True)
        subprocess.check_output(['git clone --branch {} {} $AIRFLOW_HOME/cdm'.format(cdm_config['tag'], cdm_config['url'])], shell=True)
        logging.info("succesfully cloned {} version {} into the $AIRFLOW_HOME/cdm directory".format( cdm_config['url'],  cdm_config['tag']))
    except subprocess.CalledProcessError as e:
        logging.exception("error while cloning the CDM repository")
        raise e

def prepare_queries(cdm_config: Dict):
    #TODO, i want to get the the queries from the SQL file instead of just running the SQL files
    try:
        if os.path.isdir('{}/cdm_sql'.format(os.environ['AIRFLOW_HOME'])):
            subprocess.check_call('rm -rf \"$AIRFLOW_HOME/cdm_sql\"', shell=True)
        subprocess.check_call('mkdir \"$AIRFLOW_HOME/cdm_sql\"', shell=True)
        subprocess.check_call('cp \"$AIRFLOW_HOME/cdm/PostgreSQL/OMOP CDM postgresql ddl.txt\" \"$AIRFLOW_HOME/cdm_sql/create_vocab.sql\"', shell=True)
        subprocess.check_call('cp \"$AIRFLOW_HOME/cdm/PostgreSQL/OMOP CDM postgresql ddl.txt\" \"$AIRFLOW_HOME/cdm_sql/create_cdm_database.sql\"', shell=True)
        logging.info("succesfully prepared queries for database creation")
    except subprocess.CalledProcessError as e:
        logging.exception("error while preparing cdm queries")
        raise e
    #TODO meer databases