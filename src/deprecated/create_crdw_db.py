from crdw_modules.app_config import AppConfig
from crdw_modules.util.database_wrapper import DatabaseWrapper
import os
import logging

AFTER_VOCAB_IDENTIFIER = '/**************************\n\nStandardized meta-data\n\n***************************/'


def create_crdw_db(db_name: str, force_reload: bool = False):
    db_config = AppConfig().get_config(db_name + '_database')
    db_wrapper = DatabaseWrapper(db_config, auto_connect=False)
    if force_reload and db_wrapper.database_exists():
        db_wrapper.drop_database()
    db_wrapper.create_database()
    db_wrapper.execute_query(split_data_from_vocab_tables())
    link_to_vocab_server(db_wrapper)
    db_wrapper.close_database_connection()
    logging.info('succesfully created database {}'.format(db_name))

def split_data_from_vocab_tables() -> str:
    sql_string = open('{}/cdm_sql/create_vocab.sql'.format(os.environ['AIRFLOW_HOME']), 'r').read()
    vocab_string = sql_string.split(AFTER_VOCAB_IDENTIFIER)
    return vocab_string[1]

def link_to_vocab_server(db_wrapper: DatabaseWrapper, target_schema: str = 'public'):
    vocab_config = AppConfig().get_config('vocab_database')
    queries = ['CREATE EXTENSION postgres_fdw;',
               'CREATE SERVER vocab_database_server FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host \'{}\', dbname \'{}\');'.format(
                   vocab_config['host'], vocab_config['database']),
               'CREATE USER MAPPING FOR CURRENT_USER SERVER vocab_database_server OPTIONS (user \'{}\', password \'{}\');'.format(
                   vocab_config['user'], vocab_config['password']),
               'IMPORT FOREIGN SCHEMA {} FROM SERVER vocab_database_server INTO {};'.format('public', target_schema)]  # hardcoded schema, make configurable?
    for query in queries:         
        db_wrapper.execute_query(query)

