from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from crdw_modules.app_config import AppConfig


dag = DAG('environment_build', description='pipeline to setup crdw database(s)',
          schedule_interval=None, start_date=datetime(2017, 3, 20), catchup=False)


def create_crdw_dbs():
    # TODO determine list of databases
    databases = ['cdm_crdw']
    for database in databases:
        # create_crdw_db(database, force_reload=True)
        pass

def create_mapping_db(): 
    pass

def create_staging_db():
    pass

def create_second_stage_db():
    pass


task_create_crdw_dbs = PythonOperator(
    task_id='task_create_crdw_dbs', python_callable=create_crdw_dbs, dag=dag)

task_create_crdw_dbs = PythonOperator(
    task_id='task_create_crdw_dbs', python_callable=create_crdw_dbs, dag=dag)


task_create_crdw_dbs
