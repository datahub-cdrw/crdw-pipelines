from crdw_modules.app_config import AppConfig
from crdw_modules.util.database_wrapper import DatabaseWrapper
import logging, typing, re


def initialize_databases() -> None:
    initialize_vocab()
    initialize_staging()
    initialize_mapping()
    initialize_crdw()
    initialize_airflow()

def initialize_airflow(): #TODO Force reload?
    config = AppConfig().get_config('airflow_database')
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    
def initialize_vocab(force_reload = True) -> None:
    config = AppConfig().get_config('vocab_database')
    schema_conf = AppConfig().get_config('database_schemas')
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    schema = schema_conf['vocab']
    if force_reload:
        database_wrapper.drop_schema(schema)
    if database_wrapper.schema_exists(schema):
        logging.info('skipping existing schema: {}'.format(schema))
    else:
        logging.info('creating new schema: {}'.format(schema))
        logging.info('creating the standardized vocabularies')
        database_wrapper.create_schema(schema)
        database_wrapper.execute_query(_perform_sql_for_schema(schema_conf['path'], 'standardized_vocabulary.sql', schema))
        # database_wrapper.execute_query(_perform_sql_for_schema(schema_conf['path'], 'constraints_standardized_vocabulary.sql', schema))

def initialize_staging(force_reload = True) -> None:
    schema_conf, database_wrapper = _load_config_and_create_database_wrapper()
    schema = schema_conf['staging']
    if force_reload:
        database_wrapper.drop_schema(schema)
    if database_wrapper.schema_exists(schema):
        logging.info('skipping existing schema: {}'.format(schema))
    else:
        logging.info('creating new schema: {}'.format(schema))
        database_wrapper.create_schema(schema)
        #add sql scripts here

    schema = schema_conf['staging2']
    if force_reload:
        database_wrapper.drop_schema(schema)
    if database_wrapper.schema_exists(schema):
        logging.info('skipping existing schema: {}'.format(schema))
    else:
        logging.info('creating new schema: {}'.format(schema))
        database_wrapper.create_schema(schema)
        #add sql scripts here

def initialize_mapping(force_reload = True) -> None:
    schema_conf, database_wrapper = _load_config_and_create_database_wrapper()
    schema = schema_conf['mapping']
    if force_reload:
        database_wrapper.drop_schema(schema)
    if database_wrapper.schema_exists(schema):
        logging.info('skipping existing schema: {}'.format(schema))
    else:
        logging.info('creating new schema: {}'.format(schema))
        database_wrapper.create_schema(schema)
        #add sql scripts here

def initialize_atlas(force_reload = True) -> None:
    schema_conf, database_wrapper = _load_config_and_create_database_wrapper()
    schema = schema_conf['atlas']
    if force_reload:
        database_wrapper.drop_schema(schema)
    if database_wrapper.schema_exists(schema):
        logging.info('skipping existing schema: {}'.format(schema))
    else:
        logging.info('creating new schema: {}'.format(schema))
        database_wrapper.create_schema(schema)
        #add sql scripts here

def initialize_cdm(force_reload = True) -> None:
    schema_conf, database_wrapper = _load_config_and_create_database_wrapper()
    schemas = schema_conf['cdm'].split(',')
    for schema in schemas:
        file_names = ['standardized_clinical_data.sql', 'standardized_derived_elements.sql', 'standardized_health_economics.sql', 'standardized_health_system_data.sql', 'standardized_meta_data.sql']
        _execute_sql_files_for_schema(database_wrapper, schema, schema_conf['path'], file_names, force_reload)

            #TODO add constrains here.

def apply_constraints_to_cdm() -> None:
    schema_conf, database_wrapper = _load_config_and_create_database_wrapper()
    schemas = schema_conf['cdm'].split(',')
    for schema in schemas:
        logging.info('applying constrains to the standardized clinical data tables for shema: {}'.format(schema))
        constrains_query =_perform_sql_for_schema(schema_conf['path'], 'constraints_all_tables.sql', schema)
        constrains_query = re.sub('SCHEMA_VOCAB_PLACEHOLDER', schema_conf['vocab'], constrains_query)
        database_wrapper.execute_query(constrains_query)

def remove_constraints_from_cdm() -> None:
    pass
    #Todo, do we need this?

def _execute_sql_files_for_schema(database_wrapper: 'DatabaseWrapper', schema: str, sql_path:str, file_names: typing.List[str], force_reload: bool = False) -> None:
    if force_reload:
        database_wrapper.drop_schema(schema)
    elif database_wrapper.schema_exists(schema):
        logging.info('skipping existing schema: {}'.format(schema))
    else:
        logging.info('creating new schema: {}'.format(schema))
        database_wrapper.create_schema(schema)
        for file_name in file_names:
            database_wrapper.execute_query(_perform_sql_for_schema(sql_path, file_name, schema))

def _load_config_and_create_database_wrapper() -> typing.Tuple[typing.List, 'DatabaseWrapper']:
    config = AppConfig().get_config('crdw_database')
    schema_conf = AppConfig().get_config('database_schemas')
    database_wrapper = DatabaseWrapper(config, auto_connect=True, auto_create=True)
    return schema_conf, database_wrapper

def _perform_sql_for_schema(path_in_container: str, file_name: str, schema: str) -> str:
    query_str = open('{}/common_data_model/{}'.format(path_in_container, file_name), 'r').read()
    query_str = re.sub('SCHEMA_PLACEHOLDER', schema, query_str)
    return query_str
    