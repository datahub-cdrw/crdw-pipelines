import os
import configparser
import logging
from typing import List, Dict


class AppConfig():
    INI_LOCATION_KEY = 'CRDW_INI_LOCATION'
    __configparser: configparser
    __config: Dict
    keys: List


    def __init__(self, mode: str = 'production', file_location: str = '/opt/config.ini'):
        if 'development' == mode:
            self.read_ini_file(file_location)
        else:
            self.get_ini_location_from_os_environment()

    def read_ini_file(self, file_location: str):
        self.__configparser = configparser.ConfigParser()
        self.__configparser.read_file(open(file_location))
        self.__config = dict(self.__configparser._sections)
        self.keys = list(self.__config.keys())

    def get_ini_location_from_os_environment(self):
        if self.INI_LOCATION_KEY in os.environ:
            logging.debug('Using configuration at location: ' +
                         os.environ[self.INI_LOCATION_KEY])
            self.read_ini_file(os.environ[self.INI_LOCATION_KEY])
        else:
            logging.error('invalid configuration')
            raise(FileNotFoundError('could not open ini file:', self.INI_LOCATION_KEY))

    def is_valid_key(self, keyword: str) -> bool:
        if keyword in self.keys:
            return True
        return False
  
    def get_config(self, keyword:str) -> Dict:
        if not self.is_valid_key(keyword):
            raise ValueError('config not found for keyword: ' + keyword)
        return self.__config[keyword]
