from crdw_modules.util.download_file_from_onedrive import download_file_from_onedrive
from crdw_modules.app_config import AppConfig
from crdw_modules.util.database_wrapper import DatabaseWrapper
from crdw_modules.cdm.create_new_datamart import create_new_datamart
import logging, os, zipfile


def synpuff_loader() -> None:
    _download_and_unzip()
    create_new_datamart('dm_synpuff', force_reload=True)
    _insert_into_database()
    os.system('rm /tmp/synpuff.zip')
    os.system('rm -rf /tmp/synpuff')

def _download_and_unzip() -> None:
    cdm_config = AppConfig().get_config('cdm_version_config')
    download_file_from_onedrive(cdm_config['synpuff'], '/tmp/synpuff.zip')
    if os.path.isdir('/tmp/synpuff'):
        os.system('rm -rf /tmp/synpuff')
    os.mkdir('/tmp/synpuff')
    with zipfile.ZipFile('/tmp/synpuff.zip', 'r') as zip_ref:
        zip_ref.extractall('/tmp/synpuff')

def _insert_into_database() -> None:
    database_wrapper = DatabaseWrapper(AppConfig().get_config('crdw_database'))
    for file in os.listdir("/tmp/synpuff"):
        if file.endswith(".csv"):
            _insert_csv_into_table(database_wrapper, file)

def _insert_csv_into_table(database_wrapper: 'DatabaseWrapper', file: str, schema: str = 'dm_synpuff') -> None:
    tmp = file.split('.')
    table = tmp[0]
    logging.debug('inserting file: /tmp/synpuff/{} into table {}.{}'.format(file, schema, table))
    database_wrapper.copy_from_csv('/tmp/synpuff/{}'.format(file), '\t', '{}.{}'.format(schema, table))
            