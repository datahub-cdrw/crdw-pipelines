import psycopg2,json,logging,csv, functools
from typing import Dict, List, IO, Tuple
from crdw_modules.util.StringIteratorIO import StringIteratorIO


class DatabaseWrapper():
    __connection = None
    __cursor = None
    __database_config: Dict[str, str]

    def __init__(self, database_config: Dict[str, str] = None, auto_connect: bool = True, auto_create: bool = False):
        if isinstance(database_config, dict):
            logging.debug('database connection with config: ' +
                          json.dumps(database_config))
            self.__database_config = database_config
            if auto_connect:
                if self.database_exists():
                    self.open_database_connection()
                else:
                    if auto_create:
                        self.create_database()
                        self.open_database_connection()
                    else:
                        raise ValueError('database does not exist')
        else:
            raise ValueError(
                'database_config should be type dict and is type ', database_config)

    def open_database_connection(self) -> None:
        if not self.is_connected():
            try:
                self.__connection = psycopg2.connect(**self.__database_config)
                self.__cursor = self.__connection.cursor()
                logging.debug(self.__connection.get_dsn_parameters())
            except (Exception, psycopg2.Error) as error:
                if(self.__connection):
                    self.__cursor.close()
                    self.__connection.close()
                logging.exception('Error while connecting to PostgreSQL')
                raise error

    def is_connected(self) -> bool:
        if (self.__connection == None) or (self.__connection.closed == 1):
            return False
        return True

    def database_exists(self) -> bool:
        database = self.__database_config['database']
        self.__database_config.pop('database', None)
        self.open_database_connection()
        databases = self.execute_select_query('SELECT datname FROM pg_database')
        self.close_database_connection()
        self.__database_config['database'] = database
        if self.__database_config['database'] in databases.__str__():
            return True
        else:
            return False

    def close_database_connection(self) -> None:
        if self.is_connected():
            self.__connection.close()

    def drop_database(self) -> None:
        self.close_database_connection()

        db_name_tmp = self.__database_config['database']
        del(self.__database_config['database'])
        self.open_database_connection()
        try:
            self.__cursor.connection.set_isolation_level(0)
            self.__cursor.execute('DROP DATABASE \"' + db_name_tmp + '\"')
            self.__cursor.connection.set_isolation_level(1)
        except (Exception, psycopg2.Error) as error:
            logging.exception('Error while connecting to executing query')
            raise error
        self.close_database_connection()
        self.__database_config['database'] = db_name_tmp
        logging.info('succesfully dropped database: {}'.format(db_name_tmp))

    def create_database(self) -> None:
        if self.is_connected():
            raise RuntimeError(
                'the database is already connected, cannot recreate database')

        db_name_tmp = self.__database_config['database']
        del(self.__database_config['database'])
        self.open_database_connection()
        try:
            self.__cursor.connection.set_isolation_level(0)
            self.__cursor.execute('CREATE DATABASE \"' + db_name_tmp + '\"')
            self.__cursor.connection.set_isolation_level(1)
        except (Exception, psycopg2.Error) as error:
            logging.exception('Error while connecting to executing query')
            raise error
        self.close_database_connection()
        self.__database_config['database'] = db_name_tmp
        self.open_database_connection()
        logging.info(
            'succesfully created and connected to database: {}'.format(db_name_tmp))

    def create_schema(self, schema_name: str) -> None:
        if not self.schema_exists(schema_name):
            self.execute_query('CREATE SCHEMA {};'.format(schema_name)) # TODO ownership?
        else:
            logging.debug('schema {} already exists, doing nothing'.format(schema_name))

    def drop_schema(self, schema_name: str) -> None:
        if self.schema_exists(schema_name):
            self.execute_query('DROP SCHEMA {};'.format(schema_name)) # TODO cascading?
        else:
            logging.debug('schema {} doesn\'t exist, doing nothing'.format(schema_name))

    def schema_exists(self, schema_name: str) -> bool:
        available_schemas = self.execute_select_query('select nspname from pg_catalog.pg_namespace')
        logging.debug('schemas in database: {}'.format(available_schemas))
        if schema_name in available_schemas.__str__():
            return True
        else:
            return False

    def execute_select_query(self, query: str) -> List:
        try:
            self.__cursor.execute(query)
            return self.__cursor.fetchall()
        except (Exception, psycopg2.Error) as error:
            logging.exception('Error while performing select query')
            raise error
        
    def execute_select_query_from_file(self, file_name: str) -> List:
        try:
            self.__cursor.execute(open(file_name, 'r').read())
            return self.__cursor.fetchall()
        except (Exception, psycopg2.Error) as error:
            logging.exception(
                'Error while performing select query from file: {}'.format(file_name))
            raise error  # should we stop if on of the files failes? maybe remove the list requirement?

    def execute_query(self, query: str) -> None:
        try:
            self.__cursor.execute(query)
            self.__connection.commit()
        except (Exception, psycopg2.Error) as error:
            logging.exception(
                'Error while executing query: {}'.format(query))
            raise error

    def execute_query_from_file(self, file_name: str) -> None:
        try:
            self.__cursor.execute(open(file_name, 'r').read())
            self.__connection.commit()
        except (Exception, psycopg2.Error) as error:
            logging.exception(
                'Error while executing query from file: {}'.format(file_name))
            raise error

    def copy_from_csv(self, file_name: str, separator: str, table_name: str) -> None:
        try: 
            logging.info('loading {} into table {}'.format(file_name, table_name))
            with open(file_name, 'r') as f:
                next(f)  # Skip the header row.
                self.__cursor.copy_from(f, table_name, sep=separator, null='')
            self.__connection.commit()
        except (Exception, psycopg2.Error) as error:
            self.__connection.rollback()
            logging.exception(
                'Error while copying file: {} to table: {}'.format(file_name, table_name))
            raise error

    def copy_from_csv_tmp_table(self, file_name: str, separator: str, table_name: str, 
                                database_columns: List[str], csv_header: List[str], destination_types: List[str],
                                primary_key: List[str], where_constraints: str) -> None:
        try: #todo, what if CSV header does not match the destination table? need a map?
            logging.info('loading {} into table {}'.format(file_name, table_name))
            with open(file_name, 'r') as f:
                next(f)  # Skip the header row.
                self._create_temp_table_from_csv(csv_header, separator, table_name, destination_types)
                self.__cursor.copy_from(f, '{}_tmp'.format(table_name), sep=separator, null='')
            self.__connection.commit()
            self._transfer_data_between_tables(table_name, '{}_tmp'.format(table_name), database_columns, csv_header, destination_types, primary_key, where_constraints)
        except (Exception, psycopg2.Error) as error:
            logging.exception(
                'Error while copying file: {} to table: {}'.format(file_name, table_name))
            raise error
        finally: #always drop temp table
            self.execute_query('drop table {}_tmp'.format(table_name))

    def get_table_properties(self, schema: str, table: str) -> Tuple[List[str], List[str]]:
        column_query = '''
        SELECT column_name,udt_name,character_maximum_length
        FROM information_schema.columns 
        WHERE table_schema = '{}' and table_name = '{}';
        '''.format(schema, table)
        result = self.execute_select_query(column_query)
        name = list()
        db_type = list()
        char_length = list()
        for line in result:
            name.append(line[0])
            db_type.append(line[1])
            char_length.append(line[2])

        # constraints_query = '''
        # SELECT conname
        # FROM pg_catalog.pg_constraint con
        #     INNER JOIN pg_catalog.pg_class rel
        #             ON rel.oid = con.conrelid
        #     INNER JOIN pg_catalog.pg_namespace nsp
        #             ON nsp.oid = connamespace
        # WHERE nsp.nspname = '{}'
        #     AND rel.relname = '{}';
        # '''.format(schema, table)
        # constraints = self.execute_select_query(constraints_query)
        return name, db_type, char_length #, constraints


    def _create_temp_table_from_csv(self, header_items: List[str], separator: str, table_name: str, destination_types: List[str]) -> None:
        create_query = 'create table {}_tmp ('.format(table_name)
        for i in range(header_items.__len__()):
            create_query = create_query + header_items[i] + ' ' + destination_types[i] + ' ,'
        create_query = create_query[:-1] + ');'
        logging.debug('create query constructed is: {}'.format(create_query))
        self.execute_query(create_query)

    def _transfer_data_between_tables(self, destination_table: str, source_table: str, 
                                database_columns: List[str], csv_header: List[str], destination_types: List[str],
                                primary_key: str, where_constraints: str) -> None:
        insert_query = 'insert into ' + destination_table + '(' + ' ,'.join(database_columns) + ') '
        select_query = 'select ' + ' ,'.join(csv_header) + ' from ' + source_table + ' ' + where_constraints + ' '
        if primary_key != None:
            conflict = 'on conflict ({}) do nothing'.format(primary_key)
        else:
            conflict = ''
        logging.debug('transfer query is: {}{}{}'.format(insert_query,select_query,conflict))
        self.execute_query('{}{}{}'.format(insert_query,select_query,conflict))