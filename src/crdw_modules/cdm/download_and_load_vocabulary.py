from crdw_modules.util.download_file_from_onedrive import download_file_from_onedrive
from crdw_modules.util.database_wrapper import DatabaseWrapper
from crdw_modules.app_config import AppConfig
import logging, typing, os

SCHEMA = 'vocab'

def load_concept(force_reload: bool = True):
    table = 'concept'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.concept(concept_id,concept_name,domain_id,vocabulary_id,concept_class_id,standard_concept,concept_code,valid_start_date,valid_end_date,invalid_reason)
        SELECT * FROM {}.concept_tmp WHERE 
            concept_id IS NOT NULL 
            AND concept_name IS NOT NULL 
            AND domain_id IS NOT NULL 
            AND vocabulary_id IS NOT NULL 
            AND concept_class_id IS NOT NULL 
            AND concept_code IS NOT NULL 
            AND valid_start_date IS NOT NULL 
            AND valid_end_date IS NOT NULL 
        ON CONFLICT (concept_id) DO NOTHING
    '''.format(SCHEMA, SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def load_concept_ancestor(force_reload: bool = True):
    table = 'concept_ancestor'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.concept_ancestor(ancestor_concept_id, descendant_concept_id, min_levels_of_separation, max_levels_of_separation)
        SELECT * FROM {}.concept_ancestor_tmp WHERE 
            ancestor_concept_id IS NOT NULL 
            AND descendant_concept_id IS NOT NULL 
            AND min_levels_of_separation IS NOT NULL
            AND max_levels_of_separation IS NOT NULL
        ON CONFLICT (ancestor_concept_id, descendant_concept_id) DO NOTHING
    '''.format(SCHEMA, SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def load_concept_class(force_reload: bool = True):
    table = 'concept_class'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.concept_class(concept_class_id, concept_class_name, concept_class_concept_id)
        SELECT * FROM {}.concept_class_tmp WHERE 
            concept_class_id IS NOT NULL 
            AND concept_class_name IS NOT NULL 
            AND concept_class_concept_id IS NOT NULL
        ON CONFLICT (concept_class_id) DO NOTHING
    '''.format(SCHEMA, SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def load_concept_relationship(force_reload: bool = True):
    table = 'concept_relationship'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.concept_relationship(concept_id_1, concept_id_2, relationship_id, valid_start_date, valid_end_date, invalid_reason)
        SELECT * FROM {}.concept_relationship_tmp WHERE 
            concept_id_1 IS NOT NULL 
            AND concept_id_2 IS NOT NULL 
            AND relationship_id IS NOT NULL
            AND valid_start_date IS NOT NULL
            AND valid_end_date IS NOT NULL
        ON CONFLICT (concept_id_1,concept_id_2,relationship_id) DO NOTHING
    '''.format(SCHEMA, SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def load_concept_synonym(force_reload: bool = True):
    table = 'concept_synonym'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.concept_synonym(concept_id, concept_synonym_name, language_concept_id)
        SELECT * FROM {}.concept_synonym_tmp WHERE 
            concept_id IS NOT NULL 
            AND concept_synonym_name IS NOT NULL 
            AND language_concept_id IS NOT NULL
    '''.format(SCHEMA, SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def load_domain(force_reload: bool = True):
    table = 'domain'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.domain(domain_id, domain_name, domain_concept_id)
        SELECT * FROM {}.domain_tmp WHERE 
            domain_id IS NOT NULL 
            AND domain_name IS NOT NULL 
            AND domain_concept_id IS NOT NULL
        ON CONFLICT (domain_id) DO NOTHING
    '''.format(SCHEMA, SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def load_drug_strength(force_reload: bool = True):
    table = 'drug_strength'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.drug_strength(drug_concept_id, ingredient_concept_id, amount_value, amount_unit_concept_id, numerator_value, 
            numerator_unit_concept_id, denominator_value, denominator_unit_concept_id, box_size, valid_start_date, valid_end_date, invalid_reason)
        SELECT * FROM {}.drug_strength_tmp WHERE 
            drug_concept_id IS NOT NULL 
            AND ingredient_concept_id IS NOT NULL 
            AND valid_start_date IS NOT NULL
            AND valid_end_date IS NOT NULL
        ON CONFLICT (drug_concept_id, ingredient_concept_id) DO NOTHING
    '''.format(SCHEMA, SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def load_relationship(force_reload: bool = True):
    table = 'relationship'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.relationship(relationship_id, relationship_name, is_hierarchical, defines_ancestry, reverse_relationship_id, relationship_concept_id)
        SELECT * FROM {}.relationship_tmp WHERE 
            relationship_id IS NOT NULL 
            AND relationship_name IS NOT NULL 
            AND is_hierarchical IS NOT NULL
            AND defines_ancestry IS NOT NULL 
            AND reverse_relationship_id IS NOT NULL
            AND relationship_concept_id IS NOT NULL 
        ON CONFLICT (relationship_id) DO NOTHING
    '''.format(SCHEMA, SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def load_vocabulary(force_reload: bool = True):
    table = 'vocabulary'
    database_wrapper = DatabaseWrapper(AppConfig().get_config('vocab_database'))
    if force_reload:
        database_wrapper.execute_query('delete from {}.{}'.format(SCHEMA, table))
    _download_cdm_csv(table)
    insert_query = '''
        INSERT INTO {}.vocabulary(vocabulary_id, vocabulary_name, vocabulary_reference, vocabulary_version, vocabulary_concept_id)
        SELECT * FROM {}.vocabulary WHERE vocabulary_id IS NOT NULL 
                                        AND vocabulary_name IS NOT NULL 
                                        AND vocabulary_reference IS NOT NULL 
                                        AND vocabulary_version IS NOT NULL 
                                        AND vocabulary_concept_id IS NOT NULL
        ON CONFLICT (vocabulary_id) DO NOTHING
    '''.format(SCHEMA,SCHEMA)
    _load_csv_to_database(database_wrapper, table, SCHEMA, insert_query)

def _download_cdm_csv(table: str) -> None:
    cdm_config = AppConfig().get_config('cdm_version_config')
    if table.lower() in cdm_config.keys():
        download_file_from_onedrive(cdm_config[table.lower()], '/tmp/{}.csv'.format(table.lower()))
        logging.debug(os.path.isfile('/tmp/{}.csv'.format(table.lower())))
    else:
        logging.warning('Skipped import of vocab {}, download not available'.format(table))
        raise(RuntimeError('cannot download file for {} -> missing config'.format(table)))

def _load_csv_to_database(database_wrapper: 'DatabaseWrapper', table: str, schema: str, insert_query:str):
    _create_tmp_table(database_wrapper, '{}.{}'.format(schema, table))
    database_wrapper.copy_from_csv('/tmp/{}.csv'.format(table.lower()), '\t', '{}.{}_tmp'.format(schema, table))
    database_wrapper.execute_query(insert_query)
    database_wrapper.execute_query('DROP TABLE {}.{}_tmp CASCADE'.format(schema, table))
    os.system('rm /tmp/{}.csv'.format(table.lower()))

def _create_tmp_table(database_wrapper: 'DatabaseWrapper', schema_table: str):
    database_wrapper.execute_query('CREATE TABLE {}_tmp (LIKE {} excluding constraints)'.format(schema_table, schema_table))
    database_wrapper.execute_query('''
        DO
        $$
        BEGIN

        EXECUTE (
        SELECT 'ALTER TABLE {}_tmp ALTER '
            || string_agg (quote_ident(attname), ' DROP NOT NULL, ALTER ')
            || ' DROP NOT NULL'
        FROM   pg_catalog.pg_attribute
        WHERE  attrelid = '{}_tmp'::regclass
        AND    attnotnull
        AND    NOT attisdropped
        AND    attnum > 0
        );

        END
        $$
    '''.format(schema_table, schema_table))