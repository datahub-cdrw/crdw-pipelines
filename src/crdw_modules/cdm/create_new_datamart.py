from crdw_modules.util.database_wrapper import DatabaseWrapper
from crdw_modules.app_config import AppConfig
import logging, re, typing

FILES = ['standardized_clinical_data.sql', 
        'standardized_derived_elements.sql', 
        'standardized_health_economics.sql', 
        'standardized_health_system_data.sql', 
        'standardized_meta_data.sql']

CONSTRAINT_FILES = ['constraints_all_tables.sql']

def create_new_datamart(datamart_name: str, force_reload: bool = False) -> None:
    database_wrapper, sql_path, vocab_schema = _load_config_and_create_database_wrapper() #vocab schema is here for when apply constraints is ready!
    if force_reload and database_wrapper.schema_exists(datamart_name):
        database_wrapper.execute_query('DROP SCHEMA {} CASCADE'.format(datamart_name))
    if database_wrapper.schema_exists(datamart_name):
        logging.info('skipping existing datamart_name: {}'.format(datamart_name))
    else:
        logging.info('creating new datamart_name: {}'.format(datamart_name))
        database_wrapper.create_schema(datamart_name)
        for file_name in FILES:
            database_wrapper.execute_query(_format_sql_for_schema(sql_path, file_name, datamart_name))
        #TODO add constraints
        # constraints_query =_format_sql_for_schema(sql_path, 'constraints_all_tables.sql', datamart_name)
        # constraints_query = re.sub('SCHEMA_VOCAB_PLACEHOLDER', vocab_schema, constraints_query)
        # database_wrapper.execute_query(constraints_query)

def _load_config_and_create_database_wrapper() -> typing.Tuple[str, str, 'DatabaseWrapper']:
    config = AppConfig().get_config('crdw_database')
    schema_conf = AppConfig().get_config('database_schemas')
    database_wrapper = DatabaseWrapper(config, auto_connect=True)
    return database_wrapper, schema_conf['path'], schema_conf['vocab']

def _format_sql_for_schema(path_in_container: str, file_name: str, datamart_name: str) -> str:
    query_str = open('{}/common_data_model/{}'.format(path_in_container, file_name), 'r').read()
    query_str = re.sub('SCHEMA_PLACEHOLDER', datamart_name, query_str)
    return query_str
    