FROM puckel/docker-airflow

# Airflow
ARG AIRFLOW_VERSION=1.10.9
ARG AIRFLOW_USER_HOME=/usr/local/airflow
ARG CRDW_INSTALL_LOCATION=/usr/local/crdw
ARG AIRFLOW_DEPS=
ARG PYTHON_DEPS=
ENV AIRFLOW_HOME=${AIRFLOW_USER_HOME}
ENV CRDW_LOCATION=${CRDW_INSTALL_LOCATION}
ENV CRDW_INI_LOCATION=${CRDW_INSTALL_LOCATION}/config.ini

#update container
USER root
RUN apt-get update
RUN apt-get -y install build-essential libssl-dev libcurl4-gnutls-dev libexpat1-dev gettext unzip
RUN apt-get -y install git

#Copy the dags into the container
COPY ./dags /usr/local/airflow/dags

#Copy the src into the container and install the modules with pip
COPY ./src ${CRDW_INSTALL_LOCATION}/src
COPY ./config.ini ${CRDW_INSTALL_LOCATION}/config.ini
COPY ./setup.py ${CRDW_INSTALL_LOCATION}/setup.py
RUN pip install ${CRDW_INSTALL_LOCATION}

EXPOSE 8080 5555 8793

USER airflow
WORKDIR ${AIRFLOW_USER_HOME}
ENTRYPOINT ["/entrypoint.sh"]
CMD ["webserver"]