# CRDW pipelines

the goal of this python lib is to create all logic for the crdw-etl and structure them as airflow-pipelines

## testing on your machine

### !!IMPORT !!
* **REQUIRED: DOCKER!**
* **a standard vocabulary will be downloaded from Tim's private onedrive to make testing easier**

### getting ready for unittests
**note this has to be run in the root of the repository!**
```
docker network create n_crdw
sh ./tests/scripts/postgres/setup.sh
sh ./tests/scripts/vocab/setup.sh
sh ./tests/scripts/python/setup.sh
sh ./tests/scripts/run_all_unit_tests.sh
```

you can also run a specific test file: run_specific_unit_test_file.sh *path starting from ./tests/unit/*
```
sh ./tests/scripts/run_specific_unit_test_file.sh util/database_wrapper.py
```

or you can run a specific test in a specific file: run_specific_unit_test_file.sh *path starting from ./tests/unit/* *function name*
```
sh ./tests/scripts/run_specific_unit_test_file.sh util/database_wrapper.py test_copy_from_csv
```

remove entire environment:
```
sh ./tests/scripts/remove.sh
```

remove environment:
```
sh ./tests/scripts/postgres/remove.sh
sh ./tests/scripts/vocab/remove.sh
sh ./tests/scripts/python/remove.sh
```

### getting ready for airflow tests
**note this has to be run in the root of the repository!**
**if you have run the unittests on run the last sh!**

to load load a new version of the ETL modules:
```
sh ./tests/scripts/airflow/reinstall_and_reload.sh
```

current available pipeline tests:

```
sh ./tests/scripts/airflow/run_dag_setup_omop_vocab_database.sh
sh ./tests/scripts/airflow/run_dag_setup_omop_database.sh
```

you can also go to http://localhost:8080 to look at the airflow pipeline and trigger DAGS manually.
**there will be scripted triggers comming**
